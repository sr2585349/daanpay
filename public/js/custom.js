
$(function() {
   
    if ($('.organizationCat-dataTable').length > 0) {
        var table = $('.organizationCat-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/organizationCat",
            columns: [
                { data: 'name', name: 'name' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    }
    if ($('.task-dataTable').length > 0) {
        var table = $('.task-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/task",
            columns: [
                { data: 'file', name: 'file' },
                { data: 'content', name: 'content' },
                { data: 'status', name: 'status' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    }
    if ($('.account-dataTable').length > 0) {
        var table = $('.account-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/account",
            columns: [
                { data: 'file', name: 'file' },
                { data: 'content', name: 'content' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    }
    if ($('.newsletter-dataTable').length > 0) {
        var table = $('.newsletter-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/newsletter",
            columns: [
                { data: 'email', name: 'email' },
            ]
        });
    }
    if ($('.organization-dataTable').length > 0) {
        var table = $('.organization-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/organizations",
            columns: [
                { data: 'o_name', name: 'o_name' },
                { data: 'organization_cat', name: 'organization_cat'},
                { data: 'o_mobile', name: 'o_mobile' },
                { data: 'o_email', name: 'o_email' },
                { data: 'o_pan_no', name: 'o_pan_no' },
                { data: 'auth_name', name: 'auth_name' },
                { data: 'auth_email', name: 'auth_email' },
                { data: 'whatsapp', name: 'whatsapp' },
                { data: 'pan_no', name: 'pan_no' },
                { data: 'zip', name: 'zip' },
                { data: 'city', name: 'city' },
                { data: 'state', name: 'state' },
                { data: 'gst_in', name: 'gst_in' },
                { data: 'aadhar', name: 'aadhar' },
                { data: 'letter', name: 'letter'},
                { data: 'status', name: 'status' },
                { data: 'print', name: 'print', orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    }
    if ($('.transaction-dataTable').length > 0) {
        var table = $('.transaction-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/transaction",
            columns: [
                { data: 'donator_id', name: 'donator_id' },
                { data: 'organization_name', name: 'organization_name'},
                { data: 'fname', name: 'fname' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'city', name: 'city' },
                { data: 'state', name: 'state' },
                { data: 'zip', name: 'zip' },
                { data: 'country', name: 'country' },
                { data: 'address', name: 'address' },
                { data: 'amount', name: 'amount' },
                { data: 'date', name: 'date' },
                { data: 'payment_method', name: 'payment_method' },
                { data: 'payment_status', name: 'payment_status' },
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ]
        });
    }
    if ($('.statement-dataTable').length > 0) {
        var date = $("#data_date").val();
        var table = $('.statement-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/report/request/" + date,
            columns: [
                { data: 'donator_id', name: 'donator_id' },
                { data: 'organization_name', name: 'organization_name'},
                { data: 'fname', name: 'fname' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'city', name: 'city' },
                { data: 'state', name: 'state' },
                { data: 'zip', name: 'zip' },
                { data: 'country', name: 'country' },
                { data: 'address', name: 'address' },
                { data: 'amount', name: 'amount' },
                { data: 'date', name: 'date' },
                { data: 'payment_method', name: 'payment_method' },
                { data: 'payment_status', name: 'payment_status' },
            ]
        });
    }
    if ($('.monthlyStatement-dataTable').length > 0) {
        var date = $("#data_date").val();
        var table = $('.monthlyStatement-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/monthly-report/" + date,
            columns: [
                { data: 'donator_id', name: 'donator_id' },
                { data: 'organization_name', name: 'organization_name'},
                { data: 'fname', name: 'fname' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'city', name: 'city' },
                { data: 'state', name: 'state' },
                { data: 'zip', name: 'zip' },
                { data: 'country', name: 'country' },
                { data: 'address', name: 'address' },
                { data: 'amount', name: 'amount' },
                { data: 'date', name: 'date' },
                { data: 'payment_method', name: 'payment_method' },
                { data: 'payment_status', name: 'payment_status' },
            ]
        });
    }
    if ($('.financialYear-dataTable').length > 0) {
        var date = $("#data_date").val();
        var table = $('.financialYear-dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "/admin/year-report/" + date,
            columns: [
                { data: 'donator_id', name: 'donator_id' },
                { data: 'organization_name', name: 'organization_name'},
                { data: 'fname', name: 'fname' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'city', name: 'city' },
                { data: 'state', name: 'state' },
                { data: 'zip', name: 'zip' },
                { data: 'country', name: 'country' },
                { data: 'address', name: 'address' },
                { data: 'amount', name: 'amount' },
                { data: 'date', name: 'date' },
                { data: 'payment_method', name: 'payment_method' },
                { data: 'payment_status', name: 'payment_status' },
            ]
        });
    }
});






