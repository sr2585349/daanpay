<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrganizationRejected extends Mailable
{
    use Queueable, SerializesModels;

    protected $organizationName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($organizationName)
    {
        $this->organizationName = $organizationName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Organization Application Status')
                    ->view('email.organization_rejected')
                    ->with([
                        'organizationName' => $this->organizationName,
                    ]);
    }
}
