<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;


    public static function saveData($dataVal, $fileName, $id = null)
    {
        $saveData = ($id) ? Task::find($id) : new Task;
        $saveData->content = $dataVal->content;
        if($fileName):
            $saveData->file = $fileName;
        endif;
        $saveData->save();
        return $saveData;
    }
}
