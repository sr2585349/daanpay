<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;


    public static function saveData($dataVal, $fileName, $id = null)
    {
        $saveData = ($id) ? Account::find($id) : new Account;
        $saveData->content = $dataVal->content;
        if($fileName):
            $saveData->file = $fileName;
        endif;
        $saveData->save();
        return $saveData;
    }
}
