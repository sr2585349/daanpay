<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Organization extends Model implements AuthenticatableContract
{
    use HasFactory;
    use Authenticatable;

    protected $guard = 'organiser';

    public static function saveData($dataVal,$o_photo, $id = null)
    {
        $saveData = ($id)? Organization::find($id): new Organization;
        $saveData->o_name = $dataVal->o_name;
        $saveData->o_email = $dataVal->o_email;
        $saveData->o_pan_no = strtoupper($dataVal->o_pan_no);
        $saveData->gst_in = $dataVal->gst_in;
        $saveData->organization_cat = $dataVal->organization_cat;
        $saveData->o_mobile = $dataVal->o_mobile;
        $saveData->ac_no =  $dataVal->ac_no;
        $saveData->ifsc = $dataVal->ifsc;
        $saveData->zip = $dataVal->zip;
        $saveData->city = $dataVal->city;
        $saveData->state = $dataVal->state;
        if($o_photo):
            $saveData->o_photo = $o_photo;
        endif;
        $saveData->save();
        return $saveData;
    }


    public static function updateData($dataVal, $letter, $id)
    {
        $update = Organization::where('id',$id)->first();
        $update->auth_name = $dataVal->auth_name;
        $update->aadhar = $dataVal->aadhar;
        $update->cin = $dataVal->cin;
        $update->auth_email = $dataVal->auth_email;
        $update->whatsapp = $dataVal->whatsapp;
        $update->pan_no = strtoupper($dataVal->pan_no);
        if($letter):
            $update->letter = $letter;
        endif;
        $update->save();
        return $update;

    }

    public static function getName($id)
    {
        $data = Organization::select('o_name')->where('id', $id)->first();
        return ($data)?$data->o_name:'';
    }
}
