<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    use HasFactory;

    public static function saveData($tsTransId, $doc,  $id = null)
    {
      $saveData = ($id)? Verification::find($id): new Verification;
      $saveData->tsTransId = $tsTransId;
      $saveData->document = $doc;
      $saveData->verified = 'No';
      $saveData->save();
    }
    public static function savepanData($tsTransId, $doc,  $id = null)
    {
      $saveData = ($id)? Verification::find($id): new Verification;
      $saveData->tsTransId = $tsTransId;
      $saveData->document = $doc;
      $saveData->verified = 'Yes';
      $saveData->save();
    }
}
