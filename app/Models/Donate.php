<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Donate extends Model implements AuthenticatableContract
{
    use HasFactory;
    use Authenticatable;

    protected $guard = 'donator';

    public static function saveData($dataVal,  $id = null)
    {
      $saveData = ($id)? Donate::find($id): new Donate;
      $saveData->donator_id = Donate::getUserId();
      $saveData->organization_name = $dataVal->organization_name;
      $saveData->save();
      return $saveData;
      
    }

    public static function getUserId()
    {
        return floor(time() - 999999999);
    }




    // public static function updateData($dataVal, $id)
    // {
        
    //     // Update or create the donation record
    //     $update = Donate::where('donator_id', $id)->first();
    //     if ($update) {
    //         $update->fname = $dataVal->fname;
    //         $update->lname = $dataVal->lname;
    //         $update->phone = $dataVal->phone;
    //         $update->email = $dataVal->email;
    //         $update->state = $dataVal->state;
    //         $update->city = $dataVal->city;
    //         $update->zip = $dataVal->zip;
    //         $update->address = $dataVal->address;
    //         $update->country = $dataVal->country;
    //         $update->save();
    //     }
    
    //     // Attempt to find an existing user by email
    //     $user = User::where('email', $dataVal->email)->first();
    
    //     // If user doesn't exist, create a new user account
    //     if (!$user) {
    //         $hashedPassword = Hash::make($dataVal->email);
    //         $user = new User();
    //         $user->name = $dataVal->fname;
    //         $user->email = $dataVal->email;
    //         $user->password = $hashedPassword;
    //         $user->mobile = $dataVal->phone;
    //         $user->user_type = 'Customer';
    //         $user->save();
    //     }
    
    //     // Log in the user
    //     Auth::login($user);
    
    //     return $update;
    // }
    public static function updateDonateData($dataVal, $phone, $id)
    {
        $update = Donate::where('donator_id',$id)->first();
        $update->payment_method = $dataVal->payment_method;
        $update->amount = $dataVal->amount;
        $update->phone = $phone;
        $update->save();
        return $update;
    }
    public static function updateStatus($dataVal, $id)
    {
        $update = Donate::where('donator_id',$id)->first();
        $update->payment_status = 'Completed';
        $update->date = date('Y-m-d');
        $update->month = date('m');
        $update->time = date('H:i');
        $currentDate = date('Y-m-d');
        $currentYear = date('Y', strtotime($currentDate));
        $startFinancialYear = ($currentYear - 1) . '-04-01'; 
        $endFinancialYear = $currentYear . '-03-31'; 
        if ($currentDate >= $startFinancialYear && $currentDate <= $endFinancialYear) {
            $financialYear = ($currentYear - 1) . '-' . $currentYear; 
        } else {
            $financialYear = $currentYear . '-' . ($currentYear + 1); 
        }
        $update->financial_year = $financialYear;
        $update->save();
    }
}
