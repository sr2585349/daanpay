<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsLetter extends Model
{
    use HasFactory;

    public static function saveData($dataVal,  $id = null)
    {
      $saveData = ($id)? NewsLetter::find($id): new NewsLetter;
      $saveData->email = $dataVal->email;
      $saveData->save();
      return $saveData;
      
    }
}
