<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\MsgApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Yajra\DataTables\DataTables;

class AccountsController extends Controller
{
    function __construct()
    {
        $this->middleware('role_or_permission:Account access', ['only' => ['index','show']]);

    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Account::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('file', function ($submission) {
                    $fileUrl = asset($submission->file);
                    return '<a href="' . $fileUrl . '" target="_blank"><img src="' . asset('assets/pdf.png') . '" alt="PDF" style="width: 50px; height: 50px;"></a>';
                })
               
                ->addColumn('action', function ($row) {
                   
                    $editUrl = route('admin.account.edit', $row->id);
                    $deleteUrl = route('admin.account.destroy', $row->id);
                    $btn = '';
                    if (auth()->user()->can('Task edit', $row)) {
                        $btn .= btnEdit($editUrl);
                    }
                    if (auth()->user()->can('Task delete', $row)) {
                        $btn .= btnDelete($deleteUrl);
                    }
                    return $btn;
                })
                ->rawColumns(['action','file'])
                ->make(true);
        }
        
        return view('admin.account.index');
    }

    public function create()
    {
        return view('admin.account.create');
    }

    public function store(Request $request)
    {
      
        $file = (new \App\Http\Controllers\CommonController)->uploadFile($request, 'file', '/account');
        Account::saveData($request, $file);
        return redirect('/admin/account')->with('success', MsgApp::SUCCESS_ADDED);
    }

    public function show($id)
    {
        $data = Account::where('id', $id)->first();
        if (File::exists(public_path($data->file))) {
            File::delete(public_path($data->file));
        }
        $data->delete();
        return redirect('/admin/account')->with('success', MsgApp::SUCCESS_DEL);
    }

    public function edit($id)
    {
        $data = Account::find($id);
        return view('admin.account.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Account::find($id);
        if (File::exists(public_path($data->file))) {
            File::delete(public_path($data->file));
        }
        $file = (new \App\Http\Controllers\CommonController)->uploadFile($request, 'file', '/task');
        Account::saveData($request, $file, $id);
        return redirect('/admin/account')->with('success', MsgApp::SUCCESS_UPD);
    }

  

    public function destroy($id)
    {
        return $id;
    }
}
