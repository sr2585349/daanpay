<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\MsgApp;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class TaskController extends Controller
{
   
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Task::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('file', function ($submission) {
                    $fileUrl = asset($submission->file);
                    return '<a href="' . $fileUrl . '" target="_blank"><img src="' . asset('assets/pdf.png') . '" alt="PDF" style="width: 50px; height: 50px;"></a>';
                })
                ->editColumn('status', function ($row) {
                    if($row->status != 'Completed')
                    {
                        return btnStatus(route('admin.task.status', $row->id));
                    }
                    else{
                        return 'Completed';
                    }
                   
                })
                
                ->addColumn('action', function ($row) {
                   
                    $editUrl = route('admin.task.edit', $row->id);
                    $deleteUrl = route('admin.task.destroy', $row->id);
                    $btn = '';
                    if (auth()->user()->can('Task edit', $row)) {
                        $btn .= btnEdit($editUrl);
                    }
                    if (auth()->user()->can('Task delete', $row)) {
                        $btn .= btnDelete($deleteUrl);
                    }
                    return $btn;
                })
                ->rawColumns(['action','file','status'])
                ->make(true);
        }
        
        return view('admin.task.index');
    }

    public function create()
    {
        return view('admin.task.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'file' => 'required|file|mimes:pdf',
            ],
            [
                'required' => ':attribute is required.',
                'regex' => ':attribute is invalid.',
            ],
            ['file' => 'File']
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }
        $file = (new \App\Http\Controllers\CommonController)->uploadFile($request, 'file', '/task');
        Task::saveData($request, $file);
        return redirect('/admin/task')->with('success', MsgApp::SUCCESS_ADDED);
    }

    public function show($id)
    {
        $data = Task::where('id', $id)->first();
        if (File::exists(public_path($data->file))) {
            File::delete(public_path($data->file));
        }
        $data->delete();
        return redirect('/admin/task')->with('success', MsgApp::SUCCESS_DEL);
    }

    public function edit($id)
    {
        $data = Task::find($id);
        return view('admin.task.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Task::find($id);
        if (File::exists(public_path($data->file))) {
            File::delete(public_path($data->file));
        }
        $file = (new \App\Http\Controllers\CommonController)->uploadFile($request, 'file', '/task');
        Task::saveData($request, $file, $id);
        return redirect('/admin/task')->with('success', MsgApp::SUCCESS_UPD);
    }

    public function updateStatus($id)
    {
        Task::where('id', $id)->update(['status' => 'Completed']);
        
        return redirect()->back()->with('success', 'Status UPdate successfully');
    }

    public function destroy($id)
    {
        return $id;
    }
}
