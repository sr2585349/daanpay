<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Donate;
use App\Models\Organization;
use App\MsgApp;
use Barryvdh\DomPDF\Facade\PDF;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class StatmentController extends Controller
{
    function __construct()
    {
        $this->middleware('role_or_permission:Statement access', ['only' => ['index','show']]);

    }
    public function index()
    {
        $page = MsgApp::Statement;
       return view('admin.statements.index',compact('page'));
    }

    public function store(Request $request)
    {
        $date = change_yyyy_mm_dd($request->date);
        return redirect('admin/report/request/'.$date);
    }

    public function monthWise(Request $request)
    {
        $month = $request->month;
        return redirect('admin/monthly-report/'.$month);
    }
    public function yealyhWise(Request $request)
    {
        $financial_yaer = $request->financial_yaer;
        return redirect('admin/year-report/'.$financial_yaer);
    }

    public static function idByName($id)
    {
        $data = Organization::find($id);
        return $data->o_name;
    }

    public function view(Request $request, $date)
    {

        $page = MsgApp::Statement;
        if ($request->ajax()) {
         $data =  Donate::select('*')->whereDate('date',$date);
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('organization_name', function ($row) {
                return $this->idByName($row->organization_name);

            })
            ->make(true);
        }
       return view('admin.statements.show',compact('page','date'));
    }

    public function monthlyview(Request $request, $month)
    {
        $page = MsgApp::Statement;
        if ($request->ajax()) {
         $data =  Donate::select('*')->where('month',$month);
         
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('organization_name', function ($row) {
                return $this->idByName($row->organization_name);
            })
            ->make(true);
        }
        return view('admin.statements.monthly_statements', compact('page', 'month'));
    }

    public function yearlyview(Request $request, $yearly)
    {

        $page = MsgApp::Statement;
        if ($request->ajax()) {
         $data =  Donate::select('*')->where('financial_year',$yearly);
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('organization_name', function ($row) {
                return $this->idByName($row->organization_name);
            })
            ->make(true);
        }
        return view('admin.statements.financial_year_report', compact('page', 'yearly'));
    }

    public function export(Request $request) 
    {
        $date = change_yyyy_mm_dd($request->date);
        $data = Donate::where('date', $date)->get();
        $pdf = PDF::loadView('pdf.date_wise',compact('data','date'));
        return $pdf->download('date_wise.pdf');
    } 

    public function monthWisePdf(Request $request) 
    {
        $month = $request->month;
        $data = Donate::where('month', $month)->get();
        $pdf = PDF::loadView('pdf.month_wise',compact('data'));
        return $pdf->download('month_wise.pdf');
    }
     
    public function yearreportPdf(Request $request) 
    {
        $financial_yaer = $request->financial_yaer;
        $data = Donate::where('financial_year', $financial_yaer)->get();
        $pdf = PDF::loadView('pdf.financial_yaer_wise',compact('data','financial_yaer'));
        return $pdf->download('financial_yaer_wise.pdf');
    } 
    
}
