<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NewsLetter;
use App\MsgApp;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;


class NewsLetterController extends Controller
{

    function __construct()
    {
        $this->middleware('role_or_permission:NewsLetter access', ['only' => ['index','show']]);

    }
  
    public function index(Request $request)
    {
        $page = MsgApp::NewsLetter;
        if ($request->ajax()) {
            $data = NewsLetter::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.newsletter.index',\compact('page'));;
    }

  
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        //
    }

  
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        //
    }
}
