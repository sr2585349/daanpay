<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Donate;
use App\Models\OrganizaionCat;
use App\Models\Organization;
use App\MsgApp;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TransactionController extends Controller
{
    function __construct()
    {
        $this->middleware('role_or_permission:Transaction access', ['only' => ['index','show']]);

    }
    public function index(Request $request)
    {
        $page = MsgApp::Transactions;
        if ($request->ajax()) {
            $data = Donate::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('organization_name', function ($row) {
                    return $this->idByName($row->organization_name);

                })
                ->addColumn('action', function ($row) {
                    // $editUrl = route('admin.blogs.edit', $row->id);
                    // $deleteUrl = route('admin.blogs.destroy', $row->id);
                    // $btn = '';
                    
                    //     $btn .= btnEdit($editUrl);
                    
                    //     $btn .= btnDelete($deleteUrl);
                    
                    // return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.transaction.index',\compact('page'));;
    }

    public static function idByName($id)
    {
        $data = Organization::find($id);
        return $data->o_name;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
