<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OrganizaionCat;
use App\MsgApp;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;




class OrganizaionCatController extends Controller
{

    function __construct()
    {
        $this->middleware('role_or_permission:Organization Category access|Organization Category create|Organization Category edit|Organization Category delete', ['only' => ['index','show']]);
        $this->middleware('role_or_permission:Organization Category create', ['only' => ['create','store']]);
        $this->middleware('role_or_permission:Organization Category edit', ['only' => ['edit','update']]);
        $this->middleware('role_or_permission:Organization Category delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $page = MsgApp::Organization;
        $user = auth()->user();
        if ($request->ajax()) {
            $data = OrganizaionCat::select('*');
            return DataTables::of($data)
                ->addIndexColumn()
                
                ->addColumn('action', function ($row) {
                $editUrl = route('admin.organizationCat.edit', $row->id);
                $deleteUrl = route('admin.organizationCat.destroy', $row->id);
                $btn = '';
                if (auth()->user()->can('Organization Category edit', $row)) {
                    $btn .= btnEdit($editUrl);
                }
                if (auth()->user()->can('Organization Category delete', $row)) {
                    $btn .= btnDelete($deleteUrl);
                }
                return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('organization_cat.index',compact('user','page'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = auth()->user();
        $data  = null;
        $action = 'admin.organizationCat.store';
        $id = '';
        return view('organization_cat.addUpdate', compact('data', 'action', 'id','user'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
      
       
        OrganizaionCat::saveData($request->name);
        return redirect()->route('admin.organizationCat.index')->with('success', MsgApp::SUCCESS_ADDED);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        OrganizaionCat::where('id', '=', $id)->delete();
        return redirect()->route('admin.organizationCat.index')->with('success', MsgApp::SUCCESS_DEL);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $user = auth()->user();
        $data = OrganizaionCat::where('id', $id)->first();
        $action = 'admin.organizationCat.update';
        return view('organization_cat.addUpdate', compact('data', 'action', 'id','user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
       
        OrganizaionCat::saveData($request->name,$id);
        return redirect()->route('admin.organizationCat.index')->with('success', MsgApp::SUCCESS_UPD);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        
       return $id;
    }
}
