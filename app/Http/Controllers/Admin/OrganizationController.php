<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\OrganizationApproved;
use App\Mail\OrganizationRejected;
use App\Models\OrganizaionCat;
use App\Models\Organization;
use App\MsgApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;
use Barryvdh\DomPDF\Facade\PDF;

class OrganizationController extends Controller
{
    function __construct()
    {
        $this->middleware('role_or_permission:Organization access', ['only' => ['index','show']]);

    }
    public function index(Request $request)
    {
        $page = MsgApp::Organizations;
        $user = auth()->user();
        if ($request->ajax()) {
            $data = Organization::select('*');
            return DataTables::of($data)
                ->addIndexColumn()

                ->editColumn('organization_cat', function ($row) {
                    return $this->idByName($row->organization_cat);

                })
              
                ->addColumn('upload_o_pan', function ($submission) {
                    $fileUrl = asset($submission->upload_o_pan);
                    return '<a href="' . $fileUrl . '" target="_blank"><img src="' . asset('assets/pdf.png') . '" alt="PDF" style="width: 50px; height: 50px;"></a>';
                })
                ->addColumn('letter', function ($submission) {
                    $fileUrl = asset($submission->letter);
                    return '<a href="' . $fileUrl . '" target="_blank"><img src="' . asset('assets/pdf.png') . '" alt="PDF" style="width: 50px; height: 50px;"></a>';
                })
                ->addColumn('print', function ($row) {
                    return '<a class="btn btn-success m-2" href="' . route('admin.organizations.print', $row->id) . '">Print</a>';
                })
                ->addColumn('action', function ($row) {
                    if ($row->status != 'Approved') {
                        $btn = '<a class="btn btn-success m-2" href="' . route('admin.organizations.approve', $row->id) . '">Approve</a>';
                    }
                        elseif($row->status != 'Rejected')
                        {
                            $btn = '<a class="btn btn-danger m-2" href="' . route('admin.organizations.reject', $row->id) . '">Reject</a>';

                        }
                    else {
                        $btn = btnSuspend(route('admin.organizations.Suspend', $row->id));

                    }
                    return $btn;
                    
                })
                ->rawColumns(['print','action','upload_o_pan','letter'])
                ->make(true);
        }
        return view('admin.organization.index',compact('user','page'));
    }


    public function create()
    {
      
    }

    public static function idByName($id)
    {
        $data = OrganizaionCat::find($id);
        return $data->name;
    }

    public function store(Request $request)
    {
      
      
    }

    public function show($id)
    {
        Organization::where('id', $id)->delete();
        return redirect()->route('admin.organization.index')->with('success', MsgApp::SUCCESS_DEL);
    }

    public function edit($id)
    {
      
    }

    public function update(Request $request, $id)
    {
       
    }

    public function destroy($id)
    {
        
       return $id;
    }
    public function approve($id)
    {
        Organization::where('id', $id)->update(['status' => 'Approved']);
        $organization = Organization::findOrFail($id);
        $organizationName = $organization->o_name; 
        $organizationEmail = $organization->o_email; 
        
        Mail::to($organizationEmail)->send(new OrganizationApproved($organizationName));  
        return redirect()->back()->with('success', 'Organization approved successfully');
    }
    public function reject($id)
    {

        Organization::where('id', $id)->update(['status' => 'Rejected']);
        
        
        $organization = Organization::findOrFail($id);
        $organizationName = $organization->o_name; 
        $organizationEmail = $organization->o_email; 
        
        Mail::to($organizationEmail)->send(new OrganizationRejected($organizationName));   
         return redirect()->back()->with('success', 'Organization Rejected successfully');
    }
    public function Suspend($id)
    {
        Organization::where('id', $id)->update(['status' => 'Suspended']);
        
        return redirect()->back()->with('success', 'Organization approved successfully');
    }


    public function print($id)
    {
        $data = Organization::where('id', $id)->first();
        $pdf = PDF::loadView('pdf.organization',compact('data'));
        return $pdf->download('organization.pdf');
    }

    public function  printAll()
    {
        $datas = Organization::get();
        $pdf = PDF::loadView('pdf.organizationAll',compact('datas'));
        return $pdf->download('organization-list.pdf');
    }
    


}
