<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Donate;
use App\Models\NewsLetter;
use App\Models\OrganizaionCat;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        return view('front.home');
    }
    public function about()
    {
        return view('front.about');
    }
    public function contact()
    {
        return view('front.contact');
    }
    public function donatate_step_one()
    {
        $organizationCat = OrganizaionCat::get();
        return view('front.donate_one',compact('organizationCat'));
    }
    public function donatate_step_two()
    {
        return view('front.donate_two');
    }
    public function donatate_step_three()
    {
        return view('front.donate_three');
    }
    public function account_verify($id)
    {
        $data = Organization::where('id',$id)->first();
        return view('front.account_verify',compact('data'));
    }
    public function bank_verify($id)
    {   
        
        return view('front.bank_verify',compact('id'));
    }

    public function Option()
    {
        return view('front.option');
    }
    public function userRegister()
    {
        return view('front.user_register');
    }
    public function dash()
    {
        return view('front.dash');
    }
    public function dashboard()
    {
        $donations = Donate::where('phone',auth('donator')->user()->phone)->get();
        return view('front.dashboard',compact('donations'));
    }

    public function userRegisteration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'mobile' => 'required|regex:/^[6-9][0-9]{9}$/',
            'password'=>'required'
        ],
        [
          'required' => ':attribute is required.',
          'regex' => ':attribute is invalid.',
      ],
        ['name' => 'Name','email'=>'Email','mobile'=>'Mobile','password'=>'Password']
      );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

         User::createUser($request);

        return redirect()->route('login')->with("success", 'Your Signup Successfully');

    }


    public function organizationDashboard()
    {
        return view('front.dash');
    }
    public function termCondition()
    {
        return view('front.term');
    }
    public function privacyPolicy()
    {
        return view('front.privacy_policy');
    }


    public function subscribe(Request $request)
    {
        // Validate the request
        $request->validate([
            'email' => 'required|email', // Example validation rule
        ]);

        NewsLetter::saveData($request);
        return response()->json(['message' => 'Subscription successful!']);
    }

}
