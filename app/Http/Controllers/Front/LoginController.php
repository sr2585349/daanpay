<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\OrganizaionCat;
use App\Models\Organization;
use App\Models\Verification;
use App\MsgApp;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session as FacadesSession;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function register()
    {
      $organizationCat = OrganizaionCat::get();
      return view('front.register',compact('organizationCat'));
    }

    public function loginPage()
    {
      return view('front.loginPage');
    }

    public function register_save(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'o_name' => 'required|string',
            'o_email' => 'required|email|unique:organizations',
            'o_pan_no' => 'required|regex:/^[A-Za-z]{5}[0-9]{4}[A-Za-z]$/|unique:organizations',
            // 'o_aadhar' => 'required|regex:/^\d{4}\s\d{4}\s\d{4}$/|unique:organizations',
            // 'upload_o_pan' => 'required|file|mimes:pdf,jpg,jpeg,png',
            'o_photo' => 'required|file|mimes:jpg,jpeg,png',
            'o_mobile' => 'required|regex:/^[6-9][0-9]{9}$/|unique:organizations',
            'organization_cat' => 'required',
            'gst_in' => 'required|string',
            'ifsc' => 'required',
            'ac_no' => 'required',
        ], [
            'required' => ':attribute is required.',
            'regex' => ':attribute is invalid.',
        ], [
            'o_name' => 'Name',
            'o_email' => 'Email',
            'o_pan_no' => 'Pan Number',
            // 'upload_o_pan' => 'PAN',
            'gst_in' => 'Gst In',
            'o_photo' => 'Photo',
            'o_mobile' => 'Mobile',
            'organization_cat' => 'Category',
            'ifsc' =>'IFSC',
            'ac_no' =>'Account Number'
        ]);
    
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }
    
        $o_photo = (new \App\Http\Controllers\CommonController)->uploadFile($request, 'o_photo', '/documents');
            
        $verify_email = Verification::where('document', $request->o_email)->where('verified', 'Yes')->first();


        if (!$verify_email) {
            $message = 'Please verify Organization Email.';
            return response()->json(['message' => $message], 422);
        }
        $verify_gst_in = Verification::where('document', $request->gst_in)->where('verified', 'Yes')->first();


        if (!$verify_gst_in) {
            $message = 'Please verify Gst In.';
            return response()->json(['message' => $message], 422);
        }
        $o_pan_no = strtoupper($request->input('o_pan_no'));

        $verify_pan = Verification::where('document', $o_pan_no)->where('verified', 'Yes')->first();

        if (!$verify_pan) {
            $message = 'Please verify Pan.';
            return response()->json(['message' => $message], 422);
        }
        $verify_mobile = Verification::where('document', $request->o_mobile)->where('verified', 'Yes')->first();

        if (!$verify_mobile) {
            $message = 'Please verify Mobile.';
            return response()->json(['message' => $message], 422);
        }

        $verify_bank = Verification::where('document', $request->ac_no)->where('verified', 'Yes')->first();

        if (!$verify_bank) {
            $message = 'Please verify Bank Account.';
            return response()->json(['message' => $message], 422);
        }
    
        $data = Organization::saveData($request, $o_photo);
    
        return response()->json(['message' => 'Organization Registered Successfully', 'data' => $data->id], 200);
    }
    

    public function saveAccountData(Request $request)
    {
     $id = $request->id;
        // Validate incoming request
        $validator = Validator::make($request->all(), [
            'auth_name' => 'required|string',
            'aadhar' => 'required|regex:/^\d{4}\s\d{4}\s\d{4}$/|unique:organizations',
            'pan_no' => 'required|regex:/^[A-Za-z]{5}[0-9]{4}[A-Za-z]$/|unique:organizations',
            'cin' => 'required|string',
            'letter' => 'required|file|mimes:pdf',
            'auth_email' => 'required|email',
            'whatsapp' => 'required|string',
        ]);
        if ($validator->fails()) {
          return response()->json(['errors' => $validator->errors()], 422);
      }

        $letter = (new \App\Http\Controllers\CommonController)->uploadFile($request, 'letter', '/documents');
         // Check Aadhar verification status
         $request->merge(['aadhar' => str_replace(' ', '', $request->aadhar)]);

         $aadhar = $request->input('aadhar');

         $verify = Verification::where('document', $aadhar)->where('verified', 'Yes')->first();

    
         if (!$verify) {
           $message = 'Please verify Aadhar';
             return response()->json(['message' => $message], 422);
         }

         $verify_pan = Verification::where('document', $request->pan_no)->where('verified', 'Yes')->first();

         if (!$verify_pan) {
             $message = 'Please verify Pan.';
             return response()->json(['message' => $message], 422);
         }

       
         $verify_cin = Verification::where('document', $request->cin)->where('verified', 'Yes')->first();

         if (!$verify_cin) {
             $message = 'Please verify CIN.';
             return response()->json(['message' => $message], 422);
         }
        $data = Organization::updateData($request, $letter, $id);

        return response()->json(['message' => 'Account Data Saved Successfully','data'=>$data->id], 200);
    }
    // public function bankData(Request $request)
    // {
    //  $id = $request->id;
    //     $validator = Validator::make($request->all(), [
    //         'ifsc' => 'required',
    //         'ac_no' => 'required',
        
    //     ]);
    //     if ($validator->fails()) {
    //       return response()->json(['errors' => $validator->errors()], 422);
    //   }

    //   $verify_bank = Verification::where('document', $request->ac_no)->where('verified', 'Yes')->first();

    //   if (!$verify_bank) {
    //       $message = 'Please verify Bank Account.';
    //       return response()->json(['message' => $message], 422);
    //   }

    //      Organization::updateBankData($request, $id);

    //     return response()->json(['message' => 'Registrtion Successfully'], 200);
    // }


    public function login(Request $request)
    {
  
      return view('front.login');
    }
    public function organizationlogin(Request $request)
    {
  
      return view('front.organizationlogin');
    }
    public function login_post(Request $request)
    {
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password ,'user_type' =>'Customer'])) {
        $request->session()->regenerate();
        if(session()->has('callback')):
          $callback = session('callback');
          session()->forget('callback');
          return redirect()->route($callback);
        endif;
        return redirect()->intended('/');
      }
      return Redirect ::back()->with('failed', 'Sorry email and password does not match.')->withInput($request->all());
    }
    public function usersignin(Request $request)
    {
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password ,'user_type' =>'Customer'])) {
        $request->session()->regenerate();
        if(session()->has('callback')):
          $callback = session('callback');
          session()->forget('callback');
          return redirect()->route($callback);
        endif;
        return redirect()->intended('/');
      }
      return Redirect ::back()->with('failed', 'Sorry email and password does not match.')->withInput($request->all());
    }

    public function donate()
    {
      return view('payment.rezorpay');
    }

    public function logout()
    {
      FacadesSession::flush();
      Auth::logout();
      return Redirect('/');
    }
}