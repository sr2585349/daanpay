<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Donate;
use App\MsgApp;
use Exception;
use Razorpay\Api\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class DonateController extends Controller
{
   public function donate(Request $request)
   {
    $validator = Validator::make(
        $request->all(),
        [
          'organization_name'=> MsgApp::REQ,
        ],
        [
          'required' => ':attribute is required.',
          'unique' => ':attribute already exists.',
          'exists' => MsgApp::INVALID_MESSAGE,
          'regex' => MsgApp::INVALID_MESSAGE,
          'between' => ':attribute must be between :min to :max.',
        ],
        ['organization_name' => 'Please Select Organization']
      );
      if ($validator->fails()) {
        return Redirect::back()->withErrors($validator)->withInput($request->all());
    }
        $data = Donate::saveData($request);
        return redirect('/donate/'.$data->donator_id)->with('success', MsgApp::SUCCESS_ADDED);

   }

   public function donateUPdate($id)
   {
    return view('front.donate_two',compact('id'));
   }



   public function donateUPdateData(Request $request, $id)
   {
    // $validator = Validator::make(
    //     $request->all(),
    //     [
    //       'organization_name'=> MsgApp::REQ,
    //     ],
    //     [
    //       'required' => ':attribute is required.',
    //       'unique' => ':attribute already exists.',
    //       'exists' => MsgApp::INVALID_MESSAGE,
    //       'regex' => MsgApp::INVALID_MESSAGE,
    //       'between' => ':attribute must be between :min to :max.',
    //     ],
    //     ['organization_name' => 'Please Select Organization']
    //   );
    //   if ($validator->fails()) {
    //     return Redirect::back()->withErrors($validator)->withInput($request->all());
    // }
        $data =  Donate::updateData($request,$id);
        return redirect('/donates/'.$data->donator_id)->with('success', MsgApp::SUCCESS_ADDED);
   }

   public function donateSave(Request $request, $id)
   {
      $phone =  $request->phone;
      $data =  Donate::updateDonateData($request,$phone,$id);

      return view('payment.rezorpay',compact('data'));
   }

   public function store(Request $request)
   {
       $input = $request->all();
        $id = $input['donator_id'];
       
       $api = new Api(env('RAZORPAY_KEY_ID'), env('RAZORPAY_KEY_SECRET'));
           $payment = $api->payment->fetch($input['razorpay_payment_id']);
           if(count($input) && !empty($input['razorpay_payment_id'])) {
                   $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount' => $payment['amount']));
                   Donate::updateStatus($response['id'],$id);
                   return redirect()->route('home');
               }
           }
   }
