<?php

namespace App\Http\Controllers;

use App\Models\Donate;
use App\Models\Organization;
use App\Models\Otp;
use App\Models\Verification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Barryvdh\DomPDF\Facade\PDF;
use Illuminate\Support\Facades\Auth;

define('CIPHER_KEY_LEN', 16);
class VerificationController extends Controller
{
    public function aadhar(Request $request)
    {   
        $request->merge(['aadhar' => str_replace(' ', '', $request->aadhar)]);

        $aadhar = $request->input('aadhar');
    
        $encrypted = $this->encrypt('Sameer@123', $aadhar, '1XXXX1', '211');
        

        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/v1/apicall/nid/aadhar_get_otp', [
            'requestData' => $encrypted,
        ]);
        

        $data = (json_decode($requestDataResponse,true));

        $decryptedResponse = $this->decrypt('Sameer@123', $data['responseData']); 

        $response = json_decode($decryptedResponse,true);

        if ($response['status'] === 1) {
            $tsTransId = $response['tsTransId'];
            $existingOtp = Otp::first();            
            if ($existingOtp) {
                $existingOtp->delete();
            }
        Otp::create(['trans_id' => $tsTransId]);
        }
        

        if ($response['status'] === 1) {
            $message = 'Verification successful. The Aadhar has been verified.';
        } else {
            $message = 'Verification failed. The Aadhar could not be verified.';
        }
        return response()->json(['message' => $message]);
    }
    public function o_aadhar(Request $request)
    {   

        
        $request->merge(['o_aadhar' => str_replace(' ', '', $request->o_aadhar)]);

        $aadhar = $request->input('o_aadhar');
        

        $encrypted = $this->encrypt('Sameer@123', $aadhar, '1XXXX1', '211');

        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/v1/apicall/nid/aadhar_get_otp', [
            'requestData' => $encrypted,
        ]);

        

        $data = (json_decode($requestDataResponse,true));

        $decryptedResponse = $this->decrypt('Sameer@123', $data['responseData']); 


        $response = json_decode($decryptedResponse,true);
        if ($response['status'] === 1) {
            $tsTransId = $response['tsTransId'];
            Verification::saveData($tsTransId,$aadhar);
            $existingOtp = Otp::first();            
            if ($existingOtp) {
                $existingOtp->delete();
            }
        Otp::create(['trans_id' => $tsTransId]);
        }
        

        if ($response['status'] === 1) {
            $message = 'Verification successful. The Aadhar has been verified.';
        } else {
            $message = 'Verification failed. The Aadhar could not be verified.';
        }
        return response()->json(['message' => $message]);
    }
    public function mobileLogin(Request $request)
    {   

        $transID = \uniqid();
        $data = (int) $transID;
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' =>$data,
            'docType' => 408,
            'docNumber' => $request->o_mobile,
        ]);

        $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/Mobile/idsearch', [
            'requestData' => $requestDataResponse,
        ]);
        

        $response = json_decode($responseDataResponses,true);


        $decryptedResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);

        $responses = json_decode($decryptedResponse,true);


        $referenceId = $responses['referenceId'];


        if ($responses['status'] === 1) {
            $message = 'OTP sent successfully';
            $referenceId = $referenceId;
            $tsTransId = $responses['tsTransId'];
        } else {
            $message = 'Failed';
        }
        return response()->json(['message' => $message,'tsTransId'=>$tsTransId,'referenceId',$referenceId]);
    }
    public function mobileotpLogin(Request $request)
    {   
      
        $user = Organization::where('o_mobile', $request->o_mobile)->first();
        $message = 'Mobile number not registered.';
        $mobile = null;
        if (!$user) {
            return response()->json([
                'message' => $message,
               
            ]);        }
    
        // Generate transaction ID
        $transID = uniqid();
        $data = (int) $transID;
    
        // Make API call to get encrypted data
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => $data,
            'docType' => 408,
            'docNumber' => $request->o_mobile,
        ]);
            $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/Mobile/idsearch', [
            'requestData' => $requestDataResponse,
        ]);
            $response = json_decode($responseDataResponses, true);
            $decryptedResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);
            $responses = json_decode($decryptedResponse, true);
    
        if ($responses['status'] === 1) {
            $message = 'OTP sent successfully';
            $referenceId = $responses['referenceId'];
            $tsTransId = $responses['tsTransId'];
            $mobile = $user->o_mobile;
        } else {
            $message = 'Failed';
            $referenceId = null;
            $tsTransId = null;
        }
    
        return response()->json(['message' => $message, 'tsTransId' => $tsTransId, 'referenceId' => $referenceId,'mobile'=>$mobile]);
    }
    public function usermobileotpLogin(Request $request)
    {   
      
        $user = Donate::where('phone', $request->o_mobile)->first();
        $message = 'Mobile number not registered.';
        $mobile = null;
        if (!$user) {
            return response()->json([
                'message' => $message,
               
            ]);        }
    
        // Generate transaction ID
        $transID = uniqid();
        $data = (int) $transID;
    
        // Make API call to get encrypted data
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => $data,
            'docType' => 408,
            'docNumber' => $request->o_mobile,
        ]);
            $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/Mobile/idsearch', [
            'requestData' => $requestDataResponse,
        ]);
            $response = json_decode($responseDataResponses, true);
            $decryptedResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);
            $responses = json_decode($decryptedResponse, true);
    
        if ($responses['status'] === 1) {
            $message = 'OTP sent successfully';
            $referenceId = $responses['referenceId'];
            $tsTransId = $responses['tsTransId'];
            $mobile = $user->o_mobile;
        } else {
            $message = 'Failed';
            $referenceId = null;
            $tsTransId = null;
        }
    
        return response()->json(['message' => $message, 'tsTransId' => $tsTransId, 'referenceId' => $referenceId,'mobile'=>$mobile]);
    }
    
    

    public function verifyOtp(Request $request)
    {
        $transID = Otp::first();
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/v1/apicall/encrypt', [
            'transId' => $transID->trans_id,
            'otp' => (int)$request->otp,
        ]);


        // dd($requestDataResponse);
        $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/v1/apicall/nid/aadhar_submit_otp', [
            'requestData' => $requestDataResponse,
        ]);


        $response = json_decode($responseDataResponses,true);


        $decryptedDataResponses = $this->getCurlResponse('https://www.truthscreen.com/v1/apicall/decrypt', [
            'responseData' => $response['responseData'],
        ]);
        
        // Check if decoding was successful
        $decryptedData = json_decode($decryptedDataResponses, true);       
        if ($decryptedData['status'] === 1) {
            Verification::where('tsTransId', $transID->trans_id)->update(['verified'=> 'Yes']);
            $message = 'Verification successful. The Aadhar has been verified.';

        } else {
            $message = 'Verification failed. The Aadhar could not be verified.';
        }
    
        
        return response()->json(['message' => $message]);
        


    }
    public function verifymobileOtp(Request $request)
    {
        $reference_id = $request->reference_id;
        $tsTransId = $request->tsTransId;
        $otp = $request->otp;
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'referenceId' => $reference_id,
            'tsTransId' => $tsTransId,
            'otp' => $otp,
        ]);
        $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/Mobile/request', [
            'requestData' => $requestDataResponse,
        ]);
        $response = json_decode($responseDataResponses,true);
        $decryptedDataResponses = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);
        
        $decryptedData = json_decode($decryptedDataResponses, true);

        if (isset($decryptedData['status']) && $decryptedData['status'] === 1) {
            $message = 'Verification successful. The Mobile has been verified.';
        } else {
            $message = 'Verification failed. The Mobile could not be verified.';
        }
        
        return response()->json(['message' => $message]);
        
        
        


    }
    public function loginmobileOtp(Request $request)
    {
        $reference_id = $request->reference_id;
        $tsTransId = $request->tsTransId;
        $otp = $request->otp;
    
        // Perform API calls to verify OTP
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'referenceId' => $reference_id,
            'tsTransId' => $tsTransId,
            'otp' => $otp,
        ]);
    
        $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/Mobile/request', [
            'requestData' => $requestDataResponse,
        ]);
    
        $response = json_decode($responseDataResponses, true);

    
        // Decrypt the response to get verification status
        $decryptedDataResponses = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);
        
        $decryptedData = json_decode($decryptedDataResponses, true);

    
        // Check if verification was successful
        if (isset($decryptedData['status']) && $decryptedData['status'] === 1) {
            // Authentication logic
            $user = Organization::where('o_mobile', $request->mobile)->first();

    
            if ($user) {
                Auth::guard('organiser')->login($user);
                return response()->json(['message' => 'Login successful']);
            } else {
                return response()->json(['message' => 'User not found'], 404);
            }
        } else {
            $message = 'Verification failed. The Mobile could not be verified.';
            return response()->json(['message' => $message], 400);
        }
    }
    public function userloginmobileOtp(Request $request)
    {
        $reference_id = $request->reference_id;
        $tsTransId = $request->tsTransId;
        $otp = $request->otp;
    
        // Perform API calls to verify OTP
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'referenceId' => $reference_id,
            'tsTransId' => $tsTransId,
            'otp' => $otp,
        ]);
    
        $responseDataResponses = $this->getCurlResponse('https://www.truthscreen.com/Mobile/request', [
            'requestData' => $requestDataResponse,
        ]);
    
        $response = json_decode($responseDataResponses, true);

    
        // Decrypt the response to get verification status
        $decryptedDataResponses = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);
        
        $decryptedData = json_decode($decryptedDataResponses, true);

            if (isset($decryptedData['status']) && $decryptedData['status'] === 1) {
            $user = Donate::where('phone', $request->mobile)->first();

    
            if ($user) {
                Auth::guard('donator')->login($user);
                return response()->json(['message' => 'Login successful']);
            } else {
                return response()->json(['message' => 'User not found'], 404);
            }
        } else {
            $message = 'Verification failed. The Mobile could not be verified.';
            return response()->json(['message' => $message], 400);
        }
    }
    
    
    public function verifyOemail(Request $request)
    {
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => '1XXXX1',
            'docType' => 370,
            'email' => $request->o_email,
        ]);

       
        $responseDataResponse = $this->getCurlResponse('https://www.truthscreen.com/api/v2.2/mailSearch', [
            'requestData' => $requestDataResponse,
        ]);

       


        $response = json_decode($responseDataResponse,true);

        $decryptedDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $response['responseData'],
        ]);

        $decryptedData = json_decode($decryptedDataResponse, true);



        if ($decryptedData['status'] == 'valid') {
            
            $message = 'Verification successful. The Email has been verified.';
            // Verification::saveData($request->email);

            } else {
            $message = 'Verification failed. The Email could not be verified.';
        }

        return response()->json(['message' => $message]);
    }
    


    private function getCurlResponse($url, $data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'username: test_daanpay@authbridge.com',
            ],
        ]);

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            return false; 
        }

        curl_close($curl);
        return $response;
    }
    private function getCurlResponses($url, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>json_encode($data),
          CURLOPT_HTTPHEADER => array(
            'username: test_daanpay@authbridge.com',
            'Content-Type: application/json',
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;
    }



    private function fixKey($key)
    {
       //128 bits
        if (strlen($key) < CIPHER_KEY_LEN) {
            //0 pad to len 16
            return str_pad("$key", CIPHER_KEY_LEN, "0");
        }
        if (strlen($key) > CIPHER_KEY_LEN) {
            //truncate to 16 bytes
            return substr($key, 0, CIPHER_KEY_LEN);
        }
        return $key;
    }

    private function getIV()
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
        return $iv;
    }

    private function encrypt($token, $aadharNo, $transID, $docType)
    {
        $iv = $this->getIV();
        $key = hash('sha512', $token, false);
        $key = substr($key, 0, 16);

        $data = json_encode([
            'transID' => $transID,
            'docType' => $docType,
            'aadharNo' => $aadharNo,
        ]);
        $encodedEncryptedData = base64_encode(openssl_encrypt($data, 'aes-128-cbc', $this->fixKey($key), OPENSSL_RAW_DATA, $iv));
        $encodedIV = base64_encode($iv);
        $encryptedPayload = $encodedEncryptedData . ":" . $encodedIV;
        
        return $encryptedPayload;
    }

    private function encryptOTP($token, $otp, $transID)
{
    $iv = $this->getIV(); // Get IV (Initialization Vector)
    $key = hash('sha512', $token, false); // Generate key from token
    $key = substr($key, 0, 16); // Truncate key to 16 bytes (128 bits)

    // Prepare data for encryption (you may need to adjust this based on the API requirements)
    $data = json_encode([
        'transID' => $transID,
        'otp' => $otp,
    ]);

    // Encrypt the data using AES-128-CBC encryption algorithm
    $encryptedData = openssl_encrypt($data, 'aes-128-cbc', $this->fixKey($key), OPENSSL_RAW_DATA, $iv);

    // Encode the encrypted data and IV (Initialization Vector) in base64 format
    $encodedEncryptedData = base64_encode($encryptedData);
    $encodedIV = base64_encode($iv);

    // Concatenate the encoded encrypted data and IV with a separator
    $encryptedPayload = $encodedEncryptedData . ":" . $encodedIV;

    return $encryptedPayload;
}


    private function decrypt($token, $data)
    {
        $parts = explode(':', $data);
        $encrypted = $parts[0];
        $iv = base64_decode($parts[1]);

        $key = hash('sha512', $token, false);
        $key = substr($key, 0, 16);

        $decryptedData = openssl_decrypt(base64_decode($encrypted), 'aes-128-cbc', $this->fixKey($key), OPENSSL_RAW_DATA, $iv);
        
        return $decryptedData;
    }

    public function encryptAndDecrypt()
    {
        $iv = $this->getIV();

        $encrypted = $this->encrypt('<token>', 'BXXXXXXXX', '1XXXXX3', '2');
        
        $decrypted = $this->decrypt('<token>', $encrypted);

        return response()->json([
            'encrypted' => $encrypted,
            'decrypted' => $decrypted
        ]);
    }


    public function getCurlResponseId($url, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'username: production@daanpay.com',
            ],
        ]);
        
        $response = curl_exec($curl);
        

        if (curl_errno($curl)) {
            return redirect()->back()->with('error', 'cURL error: ' . curl_error($curl));
        }
        curl_close($curl);
        return json_decode($response, true);

        
    }

    public function donationReciept($id)
    {
        $data = Donate::where('donator_id', $id)->firstOrFail();
        $pdf = PDF::loadView('pdf.date_wise', compact('data'));
        return $pdf->download('receipt.pdf');
    }
}
