<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\MsgApp;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends Controller
{

    public function index()
    {
        $page = MsgApp::REPORT;
       return view('report.index',compact('page'));
    }
    public function audit_report_download(Request $request)
    {
         $start = dateFormate($request->start_date);
         $end = dateFormate($request->end_date);
         return Excel::download(new ProductExport($start,$end), 'product.xlsx');  
    }
}
