<?php

namespace App\Http\Controllers;

use App\Models\Verification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PANVerificationController extends Controller
{
    public function verify(Request $request)
        {

                $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
                'transID' => '1XXXX1',
                'docType' => 2,
                'docNumber' => $request->pan_no,
            ]);
            if (isset($requestDataResponse['error'])) {
                return response()->json(['error' => 'Error retrieving requestData from the first API'], 500);
            }
            $requestData = $requestDataResponse;
            $responseDataResponse = $this->getCurlResponseId('https://www.truthscreen.com/v1/apicall/nid/idsearch', [
                'requestData' => $requestData,
            ]);
            if (isset($responseDataResponse['error'])) {
                return response()->json(['error' => 'Error retrieving responseData from the second API'], 500);
            }
            $responseData = $responseDataResponse['responseData'];
            $decryptedDataResponse = $this->getCurlResponsePan('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
                'responseData' => $responseData,
            ]);
            
            if ($decryptedDataResponse['status'] === 1) {
            $message = 'Verification successful. The PAN has been verified.';
            } else {
            $message = 'Verification failed. The PAN could not be verified.';
        }

        return response()->json(['message' => $message]);
    }

    private function getCurlResponse($url, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'username: production@daanpay.com',
            ],
        ]);

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            return false; // Return false on error
        }

        curl_close($curl);
        return $response;
    }

     public function getCurlResponseId($url, $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'username: production@daanpay.com',
            ],
        ]);
        
        $response = curl_exec($curl);
        

        if (curl_errno($curl)) {
            return redirect()->back()->with('error', 'cURL error: ' . curl_error($curl));
        }
        curl_close($curl);
        return json_decode($response, true);
    }
  public function getCurlResponsePan($url, $data)
    {
        

       $curl = curl_init();

       curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'username: production@daanpay.com',
        ),
    ));

        
        $response = curl_exec($curl);
      if (curl_errno($curl)) {
            return redirect()->back()->with('error', 'cURL error: ' . curl_error($curl));
        }
        curl_close($curl);
        return json_decode($response, true);
    }

    public function verifyopan(Request $request)
    {
        $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => '1XXX1',
            'docType' => 2,
            'docNumber' => $request->o_pan_no,
        ]);

        
        if (isset($requestDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving requestData from the first API'], 500);
        }
        $requestData = $requestDataResponse;
       

        $responseDataResponse = $this->getCurlResponseId('https://www.truthscreen.com/v1/apicall/nid/idsearch', [
            'requestData' => $requestData,
        ]);

        
        if (isset($responseDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving responseData from the second API'], 500);
        }
        $responseData = $responseDataResponse['responseData'];
        $decryptedDataResponse = $this->getCurlResponsePan('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $responseData,
        ]);
        
        if ($decryptedDataResponse['status'] === 1) {
            $tsTransId = null;
            $pan_number = strtoupper($request->o_pan_no);
            $verification = Verification::where('document', $pan_number)->first();
            if ($verification) {
                if ($verification->verified === 'No') {
                    $verification->verified = 'Yes';
                    $verification->save();
                } 
            } else {
                $tsTransId = null;
                Verification::savepanData($tsTransId, $pan_number);
            
                $message = 'Verification created successfully.';
            }           
             $message = 'Verification successful. The PAN has been verified.';
            } 
        else {
            $message = 'Verification failed. The PAN could not be verified.';
        }

        return response()->json(['message' => $message]);
    }

    public function verifygstin(Request $request)
    {

            $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => '1XXXX1',
            'docType' => 23,
            'docNumber' => $request->gst_in,
        ]);

        
        

        // Check if the response is valid
        if (isset($requestDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving requestData from the first API'], 500);
        }

        // Extract the requestData from the response
        $requestData = $requestDataResponse;
        // Call the second API with requestData
        $responseDataResponse = $this->getCurlResponseId('https://www.truthscreen.com/api/v2.2/idsearch', [
            'requestData' => $requestData,
        ]);


       
        

           // Check if the response is valid
        if (isset($responseDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving responseData from the second API'], 500);
        }
        
        // Extract the responseData from the $responseDataResponse
        $responseData = $responseDataResponse['responseData'];
        
        // Call the third API with responseData
        $decryptedDataResponse = $this->getCurlResponsePan('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $responseData,
        ]);
        
        if ($decryptedDataResponse['status'] === 1) {
            $tsTransId = null;
            $gstIn = $request->gst_in;
            $verification = Verification::where('document', $gstIn)->first();
            if ($verification) {
                if ($verification->verified === 'No') {
                    $verification->verified = 'Yes';
                    $verification->save();
                } 
            } else {
                $tsTransId = null;
                Verification::savepanData($tsTransId, $gstIn);
            
                $message = 'Verification created successfully.';
            }  

            
        $message = 'Verification successful. The GSTIN has been verified.';
    } else {
        $message = 'Verification failed. The GSTIN could not be verified.';
    }

    return response()->json(['message' => $message]);
    }
    public function verifycin(Request $request)
    {
            $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => '1XXXX1',
            'docType' => 15,
            'docNumber' => $request->cin,
            'docName' => '',
        ]);
        if (isset($requestDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving requestData from the first API'], 500);
        }
        $requestData = $requestDataResponse;
        $responseDataResponse = $this->getCurlResponseId('https://www.truthscreen.com/api/v2.2/idsearch', [
            'requestData' => $requestData,
        ]);

        if (isset($responseDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving responseData from the second API'], 500);
        }
        
        $responseData = $responseDataResponse['responseData'];

        $decryptedDataResponse = $this->getCurlResponsePan('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $responseData,
        ]);

        
        if ($decryptedDataResponse['status'] === 1) {
            $tsTransId = null;
            $cin = $request->cin;
            $verification = Verification::where('document', $cin)->first();
            if ($verification) {
                if ($verification->verified === 'No') {
                    $verification->verified = 'Yes';
                    $verification->save();
                } 
            } else {
                $tsTransId = null;
                Verification::savepanData($tsTransId, $cin);
            
                $message = 'Verification created successfully.';
            }  

        $message = 'Verification successful. The CIN has been verified.';
    } else {
        $message = 'Verification failed. The CIN could not be verified.';
    }

    return response()->json(['message' => $message]);
    }
    public function verifyAcount(Request $request)
    {
            $requestDataResponse = $this->getCurlResponse('https://www.truthscreen.com/InstantSearch/encrypted_string', [
            'transID' => '1XXXX1',
            'docType' => 92,
            'beneAccNo' => $request->ac_no,
            'ifsc' => $request->ifsc,
        ]);
        if (isset($requestDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving requestData from the first API'], 500);
        }
        $requestData = $requestDataResponse;
        $responseDataResponse = $this->getCurlResponseId('https://www.truthscreen.com/BankAccountVerificationApi', [
            'requestData' => $requestData,
        ]);
        

        if (isset($responseDataResponse['error'])) {
            return response()->json(['error' => 'Error retrieving responseData from the second API'], 500);
        }
        
        $responseData = $responseDataResponse['responseData'];

        $decryptedDataResponse = $this->getCurlResponsePan('https://www.truthscreen.com/InstantSearch/decrypt_encrypted_string', [
            'responseData' => $responseData,
        ]);

        
        if ($decryptedDataResponse['status'] === 1) {

            $tsTransId = null;
            $ac_no = $request->ac_no;
            $verification = Verification::where('document', $ac_no)->first();
            if ($verification) {
                if ($verification->verified === 'No') {
                    $verification->verified = 'Yes';
                    $verification->save();
                } 
            } else {
                $tsTransId = null;
                Verification::savepanData($tsTransId, $ac_no);
            
                $message = 'Verification created successfully.';
            }  

        $message = 'Verification successful. The Account has been verified.';
    } else {
        $message = 'Verification failed. The Account could not be verified.';
    }

    return response()->json(['message' => $message]);
    }

 

}