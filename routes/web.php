<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\{
    DashboardController,
    ProfileController,
    OrganizationController,
    StatmentController,
    TaskController,
    UserController,
};
use App\Http\Controllers\CommonController;
use App\Http\Controllers\Front\Auth\AuthenticatedSessionController as AuthAuthenticatedSessionController;
use App\Http\Controllers\Front\DonateController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\LoginController;
use App\Http\Controllers\Front\ProfileCOntroller as FrontProfileCOntroller;
use App\Http\Controllers\PANVerificationController;
use App\Http\Controllers\VerificationController;
use App\MsgApp;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[HomeController::class,'index'])->name('home');
Route::get('/about',[HomeController::class,'about'])->name('about');
Route::get('/term-condition',[HomeController::class,'termCondition'])->name('term-condition');
Route::get('/privacy-policy',[HomeController::class,'privacyPolicy'])->name('privacy-policy');
Route::get('/contact',[HomeController::class,'contact'])->name('contact');
Route::get('/donate-step-1',[HomeController::class,'donatate_step_one'])->name('donatate_step_one');
Route::get('/donate-step-2',[HomeController::class,'donatate_step_two'])->name('donatate_step_two');
Route::get('/donate-step-3',[HomeController::class,'donatate_step_three'])->name('donatate_step_three');
Route::get('/account-verify',[HomeController::class,'account_verify'])->name('account-verify');
Route::get('/bank-verify',[HomeController::class,'bank_verify'])->name('bank-verify');
Route::get('/registration',[HomeController::class,'option'])->name('option');
Route::get('/user-register',[HomeController::class,'userRegister'])->name('user-register');
Route::post('/user-registeration',[HomeController::class,'userRegisteration'])->name('userRegister');
Route::get('/account-verify/{id}',[HomeController::class,'account_verify'])->name('account-verify');
Route::get('/bank-verify/{id}',[HomeController::class,'bank_verify'])->name('bank-verify');
Route::get('/register', [LoginController::class, 'register'])->name('register');
Route::post('/sign-up', [LoginController::class, 'register_save'])->name('sign-up.stosre');
Route::post('/saveAccountData', [LoginController::class, 'saveAccountData'])->name('account-data');
Route::post('/bank-data', [LoginController::class, 'bankData'])->name('bank-data');
Route::get('/user/login', [LoginController::class, 'login'])->name('userlogin');
Route::get('/organization/login', [LoginController::class, 'organizationlogin'])->name('organizationLogin');
Route::get('/login', [LoginController::class, 'loginPage'])->name('login');
// Route::post('/signin', [LoginController::class, 'login_post'])->name('signin');
Route::post('/donate', [DonateController::class, 'donate'])->name('donate');
Route::get('/donate/{id}', [DonateController::class, 'donateUPdate']);
Route::post('/donate-update/{id}', [DonateController::class, 'donateUPdateData']);
Route::get('/donates/{id}', [DonateController::class, 'donateStepThree']);
Route::post('/donates-save/{id}', [DonateController::class, 'donateSave']);
Route::post('/thank-you/{id}', [DonateController::class, 'thankYou'])->name('thankYou');
Route::post('razorpay-payment',[DonateController::class,'store'])->name('razorpay.payment.store');

Route::post('/verify-pan', [PANVerificationController::class,'verify']);
Route::post('/verify-aadhar', [VerificationController::class,'aadhar']);
Route::post('/verify-o_aadhar', [VerificationController::class,'o_aadhar'])->name('verify.o_aadhar');
Route::post('/verify-o_email', [VerificationController::class,'verifyOemail'])->name('verify.o_email');
Route::post('/verifyOtp', [VerificationController::class,'verifyOtp'])->name('verifyOtp');
Route::post('/verify-opan', [PANVerificationController::class,'verifyopan'])->name('verify_opan');
Route::post('/verify-gstin', [PANVerificationController::class,'verifygstin'])->name('verify.gstin');
Route::post('/verify-cin', [PANVerificationController::class,'verifycin'])->name('verify.cin');
Route::post('/verify-account', [PANVerificationController::class,'verifyAcount'])->name('verify.account');
Route::post('/organization-data', [LoginController::class,'register_save'])->name('organization-data');
Route::post('/admin-login', [UserController::class, 'login_post'])->name('dashboard.login');

require __DIR__.'/auth.php';
Route::get('/dash',[HomeController::class,'dash'])->name('dash');
Route::namespace('App\Http\Controllers\Admin')->name('admin.')->prefix('admin')->middleware(['auth'])->group(function(){
        Route::resource('roles','RoleController');
        Route::resource('permissions','PermissionController');
        Route::resource('users','UserController');
        Route::resource('organizationCat','OrganizaionCatController');
        Route::resource('organizations','OrganizationController');
        Route::resource('transaction','TransactionController');
        Route::resource('statements','StatmentController');
        Route::resource('newsletter','NewsLetterController');
        Route::resource('account','AccountsController');
        Route::resource('task','TaskController');
        Route::get('/dashboard',[DashboardController::class,'index'])->name('dashboard');
        Route::post('/report/store', [StatmentController::class, 'store'])->name('report.store');
        Route::post('/month-report/store', [StatmentController::class, 'monthWise'])->name('monthreport.store');
        Route::post('/year-report/store', [StatmentController::class, 'yealyhWise'])->name('yearreport.store');
        Route::get('/report/request/{date}',[StatmentController::class, 'view'])->name('report.view');
        Route::get('/monthly-report/{month}',[StatmentController::class, 'monthlyview'])->name('report.monthlyview');
        Route::get('/year-report/{year}',[StatmentController::class, 'yearlyview'])->name('report.yearlyview');
        Route::post('/export', [StatmentController::class, 'export'])->name('datewise.export');
        Route::post('/monthWise', [StatmentController::class, 'monthWisePdf'])->name('monthWise');
        Route::post('/yearreportPdf', [StatmentController::class, 'yearreportPdf'])->name('yearreport');
        Route::get('/approve/{id}',[OrganizationController::class,'approve'])->name('organizations.approve');
        Route::get('/reject/{id}',[OrganizationController::class,'reject'])->name('organizations.reject');
        Route::get('/print/{id}',[OrganizationController::class,'print'])->name('organizations.print');
        Route::get('/printAll',[OrganizationController::class,'printAll'])->name('organizationCat.printAll');
        Route::get('/Suspend/{id}',[OrganizationController::class,'Suspend'])->name('organizations.Suspend');
        Route::get('/task/status/{id}',[TaskController::class,'updateStatus'])->name('task.status');
        Route::get('/profile',[ProfileController::class,'index'])->name('profile');
        Route::put('/profile-update',[ProfileController::class,'update'])->name('profile.update');   
        Route::get('logout', [AuthAuthenticatedSessionController::class, 'logout'])->name('admin.logout');

});
       

Route::prefix('user')->middleware('auth:donator')->group(function () {
    Route::get('/logout', [LoginController::class, 'logout'])->name('userlogout');
        Route::get('/dashboard',[HomeController::class,'dashboard'])->name('user-dashboard');
        Route::get('/donation-reciept/{id}', [VerificationController::class, 'donationReciept'])->name('donation.reciept');
    });


Route::prefix('organization')->middleware('auth:organiser')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'organizationDashboard'])->name('organizationDashboard');
    Route::get('/logout', [LoginController::class, 'logout'])->name('organizationogout');
});

Route::post('/get-data', [CommonController::class,'getData'])->name('get.data');
Route::post('/verify-mobilOtp', [VerificationController::class, 'mobileLogin'])->name('mobile.verity');
Route::post('/verifymobileOtp', [VerificationController::class, 'verifymobileOtp'])->name('verifymobileOtp');
Route::post('/userloginmobileOtp', [VerificationController::class, 'userloginmobileOtp'])->name('userloginmobileOtp');
Route::post('/verify-mobileotpLogin', [VerificationController::class, 'mobileotpLogin'])->name('mobileloginverify');
Route::post('/loginmobileOtp', [VerificationController::class, 'loginmobileOtp'])->name('loginmobileOtp');
Route::post('/verify-mobileotpLoginn', [VerificationController::class, 'usermobileotpLogin'])->name('usermobileotpLogin.verify');

Route::post('/subscribe', [HomeController::class,'subscribe'])->name('newsletter.subscribe');
