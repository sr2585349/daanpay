@extends('layouts.app')
@section('content')
@include('layouts.aside')
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">

                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                       Update Accounts </h5>
                    <!--end::Page Title-->

                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    
                    <!--end::Actions-->
                </div>
                <!--end::Info-->


            </div>
        </div>
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class=" container card-fixed-align">
        <!--begin::Card-->
        @if(session('success'))
        <div class="alert alert-custom alert-success fade show" role="alert">
            <div class="alert-icon"><i class="flaticon2-check-mark"></i></div>
            <div class="alert-text">{{ session('success')}}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="ki ki-close"></i></span>
                </button>
            </div>
        </div>
        @endif
        @if(session('failed'))
        <div class="alert alert-custom alert-danger fade show" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">{{ session('failed')}}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="ki ki-close"></i></span>
                </button>
            </div>
        </div>
        @endif

        <div class="card card-custom">
            <!--begin::Form-->
            <form action="{{route('admin.account.update', $data->id)}}" method="post" enctype="multipart/form-data">
            @method('PATCH')
                <input type="hidden" name="_token" id="csrf_token" value="{{ Session::token() }}" />
                <div class="card-body">


                    <div class="row">
                        <div class="col-md-12">
                            <label class="col-form-label" for="name">Description</label>
                            <div>
                            <textarea class="ckeditor form-control" rows="3" name="content" placeholder="Description"
                            id="description">@if(old('content')){{ old('content') }}@else{{ $data->content }}@endif </textarea>
                            
                            @if($errors->has('content'))
                               <div class="invalid-feedback">{{ $errors->first('content') }}</div>
                               @enderror
                            </div>
                        </div>
                        
                       
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <label class="col-form-label" for="image">File</label><br>
                                </div>
                                <div class="col-12">
                                    <div>
                                        <input type="file" class="custom-file-input" name="file" />
                                        <label class="custom-file-label" for="image">Choose file</label>
                                        @if($errors->has('file'))
                                        <div class="invalid-feedback">{{ $errors->first('file') }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer mt-4">
                    <div class="row">
                        <div class="col-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('admin.task.index')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
                </div>
            </form>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection