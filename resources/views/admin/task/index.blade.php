@extends('layouts.app')
@section('content')
@include('layouts.aside')
<!--begin::Entry-->
<div class="subheader py-2 py-lg-4  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-2">

                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                        Task List </h5>
                    <!--end::Page Title-->

                    <!--begin::Actions-->
                    <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                    @can('Task create')
                    <a href="{{route('admin.task.create')}}" class="btn btn-light-warning font-weight-bolder btn-sm">
                        Add New
                    </a>
                    @endcan
                    <!--end::Actions-->
                </div>
                <!--end::Info-->


            </div>
        </div>
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class=" container card-fixed-align">
        <!--begin::Card-->
        <div class="card card-custom">
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-bordered task-dataTable">
                    <thead>
                        <tr>
                            <th>File</th>
                            <th>Content</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->
@endsection

