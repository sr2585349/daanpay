<!DOCTYPE html>
<html lang="en">
<head>
  <title>Organizaion </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

  <style>
    /* Additional custom styles can be added here */
    .table-header {
      padding: 4px;
      background-color: #343a40; /* Dark background color */
      color: white;
      text-align: center;
      margin-bottom: 20px;
    }

    table {
      font-size: 5px; /* Adjust as needed */
    }

    th, td {
      font-size: 5px; /* Adjust as needed */
    }
  </style>
</head>

<body>
<div class="table-header"><h5>Organizaion List</h5></div>

    <div class="container">
        <table class="table table-bordered mb-4">
            <thead>
                <tr>
                    <th>Organization Name</th>
                    <th>Organizaion Category</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Pan</th>
                    <th>GST IN</th>
                    <th>Aadhar</th>
                    <th>IFSC</th>
                    <th>CIN</th>
                    <th>Account Number</th>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $data)
                <tr>
                    <td>{{$data->o_name}}</td>
                    <td>{{bladeOrganizationCatGetNameById($data->organization_cat)}}</td>
                    <td>{{$data->o_mobile}}</td>
                    <td>{{$data->o_email}}</td>
                    <td>{{$data->o_pan_no}}</td>
                    <td>{{$data->gst_in}}</td>
                    <td>{{$data->o_aadhar}}</td>
                    <td>{{$data->ifsc}}</td>
                    <td>{{$data->cin}}</td>
                    <td>{{$data->ac_no}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Optional: Print Button -->
    <!-- Optional: JavaScript and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
