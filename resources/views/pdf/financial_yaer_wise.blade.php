<!DOCTYPE html>
<html lang="en">
<head>
  <title>Request Report</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

  <style>
    /* Additional custom styles can be added here */
    .table-header {
      padding: 4px;
      background-color: #343a40; /* Dark background color */
      color: white;
      text-align: center;
      margin-bottom: 20px;
    }

    table {
      font-size: 10px; /* Adjust as needed */
    }

    th, td {
      font-size: 10px; /* Adjust as needed */
    }
  </style>
</head>

<body>
    <div class="table-header"><h5>Statement Financial Year ($financial_yaer)</h5></div>
    <div class="container">
        <table class="table table-bordered mb-4">
            <thead>
                <tr>
                    <th scope="col" style="width: 10%;">#</th>
                    <th scope="col" style="width: 10%;">Organization Name</th>
                    <th scope="col" style="width: 10%;">First Name</th>
                    <th scope="col" style="width: 10%;">Last Name</th>
                    <th scope="col" style="width: 10%;">Email</th>
                    <th scope="col" style="width: 10%;">Phone</th>
                    <th scope="col" style="width: 10%;">Amount</th>
                    <th scope="col" style="width: 10%;">Payment Method</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $request)
                <tr>
                    <td>{{$request->donator_id}}</td>
                    <td>{{bladeOrganizationGetNameById($request->organization_name)}}</td>
                    <td>{{$request->fname}}</td>
                    <td>{{$request->lname}}</td>
                    <td>{{$request->email}}</td>
                    <td>{{$request->phone}}</td>
                    <td>{{$request->amount}}</td>
                    <td>{{$request->payment_method}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Optional: Print Button -->
    <!-- Optional: JavaScript and Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
