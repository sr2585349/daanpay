<html  lang="en">

<head>
	<!-- Meta Tags -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="Site keywords here">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="description" content="">
	<meta name='copyright' content=''>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Favicon -->
	<link rel="icon" href="{{asset('assets/front/img/images/logo.png')}}">


	<!-- Title -->
	<title>DAANPAY-INDIA</title>

	<!-- Favicon -->
	<link rel="android-chrome" sizes="192x192" href="img/favicon-icon/android-chrome-192x192.png">
	<link rel="android-chrome" sizes="512x512" href="img/favicon-icon/android-chrome-512x512.png">
	<link rel="apple-touch-icon" sizes="180x180" href="img/favicon-icon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-icon/favicon-16x16.png">
	<link rel="manifest" href="img/favicon-icon/site.webmanifest">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
	<!-- Google Fonts -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
		rel="stylesheet">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
	<!-- Nice Select CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/nice-select.css')}}">
	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/font-awesome.min.css')}}">
	<!-- icofont CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/icofont.css')}}">
	<!-- Slicknav -->
	<link rel="stylesheet" href="{{asset('assets/front/css/slicknav.min.css')}}">
	<!-- Owl Carousel CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/owl-carousel.css')}}">
	<!-- Datepicker CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/datepicker.css')}}">
	<!-- Animate CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/animate.min.css')}}">
	<!-- Magnific Popup CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/magnific-popup.css')}}">

	<!-- Medipro CSS -->
	<link rel="stylesheet" href="{{asset('assets/front/css/normalize.css')}}">
	<link rel="stylesheet" href="{{asset('assets/front/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css">

<style>
	.goog-te-gadget .goog-te-combo {
    margin: 4px 0 0px 0px;
    font-size: 12px;
    padding: 4px 2px;
    border-radius: 3px;
    }
	</style>
</head>

<body>

	<!-- Preloader -->
	<div class="preloader">
		<div class="loader">
			<div class="loader-outter"></div>
			<div class="loader-inner"></div>

			<div class="indicator">
				<svg width="16px" height="12px">
					<polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
					<polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
				</svg>
			</div>
		</div>
	</div>
	<!-- Header Area -->
	<header class="header">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row text-center ">
					<!-- Start Logo -->
					<div class="logo text-center">
						<a href="{{route('home')}}"><img src="{{asset('assets/front/img/images/logo.png')}}" alt="#"><br><span>DAANPAY INDIA</span></a>
					</div>
					<!-- End Logo -->
				</div>
				
			</div>
		</div>
		<!-- End Topbar -->
		<!-- Header Inner -->
		<div class="header-inner">
			<div class="container-fluid">
				<div class="inner">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-12 text-center">
							<!-- Start Logo -->
							
							<!-- End Logo -->
							<ul class="social">
								<li><a href="https://www.facebook.com/daanpay/" class="facebook"><i class="icofont-facebook"></i></a></li>
								<li><a href="https://www.instagram.com/daanpay2021/" class="instagram"><i class="icofont-instagram"></i></a></li>
								<li>
									<a href="#" class="twitter">
										<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-twitter-x" viewBox="0 0 16 16">
											<path style="fill: #000;" d="M12.6.75h2.454l-5.36 6.142L16 15.25h-4.937l-3.867-5.07-4.425 5.07H.316l5.733-6.57L0 .75h5.063l3.495 4.633L12.601.75Zm-.86 13.028h1.36L4.323 2.145H2.865z"/>
										</svg>
									</a>
								</li>
								<li><a href="#" class="linkedin"><i class="icofont-linkedin"></i></a></li>
							</ul>
							<!-- Mobile Nav -->
							<div class="mobile-nav"></div>
							<!-- End Mobile Nav -->
						</div>
						<div class="col-lg-5 col-md-8 col-12 text-center">
							<!-- Main Menu -->
							<div class="main-menu">
								<nav class="navigation">
									<ul class="nav menu">
										<li class="active">
											<a href="{{route('home')}}">Home</a>
										</li>
										<li><a href="{{route('about')}}">About Us</a></li>
										
										<li><a href="{{route('contact')}}">Contact Us</a></li>
									</ul>
								</nav>
							</div>
							
						</div>
						<div class="col-lg-3 col-12">
							<div class="get-quote">
							@if(auth()->user())
							<a href="{{route('user-dashboard')}}">Hi, {{auth()->user()->name}}</a>
							<a href="{{route('userlogout')}}">Logout</a>
							@elseif(auth('organiser')->user())
							<a href="{{route('organizationDashboard')}}">Hi, {{auth('organiser')->user()->o_name}}</a>
							<a href="{{route('organizationogout')}}">Logout</a>
							@endif
							<div class="change-lang d-flex">
								<div id="google_translate_element"> </div>
								<a href="{{route('donatate_step_one')}}" class="btn mt-1">Donate</a>

							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Header Inner -->
	</header>

	
	<!-- End Header Area -->