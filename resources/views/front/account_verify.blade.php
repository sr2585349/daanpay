@extends('front.layouts.app')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>Account Verification</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li ><a href="{{route('register')}}">Registration</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">Account Verification</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    <!-- Start Contact Us -->
    <section class="contact-us section">
        <div class="container">

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="inner">
                        <div class="contact-us-form">
                            <h2>Account Verification</h2>
                            <h6 id="erroraadhar" class="text-danger"></h6>
                            <!-- Form -->
                            <form class="form" id="organizationForm" method="post" action="{{ route('account-data') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Name of Authorized</label>
                                            <input type="text" value="{{ old('auth_name') }}" name="auth_name" placeholder="Your Name">
                                            <div class="invalid-feedback" id="auth_name_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Aadhar </label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control" oninput="formatCardNumber()" value="{{ old('aadhar') }}" id="aadhar" name="aadhar" placeholder="Enter aadhar" aria-label="Example input" aria-describedby="button-addon1" />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_aadhar_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                            
                                        </div>
                                        <div class="invalid-feedback" id="aadhar_error"></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>CIN Number</label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control" value="{{ old('cin') }}" id="cin" name="cin" placeholder="CIN No."/>
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_cin_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                            
                                        </div>
                                        <div class="invalid-feedback" id="cin_error"></div>
                                    </div>
                                   
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Authority Letter (Pdf Letter)</label>
                                            <input type="file" name="letter">
                                            <div class="invalid-feedback" id="letter_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" value="{{ old('auth_email') }}"  placeholder="Enter Email" name="auth_email">
                                            <div class="invalid-feedback" id="auth_email_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Whatsapp No</label>
                                            <input type="text" value="{{ old('whatsapp') }}" placeholder="Enter whatsapp Number" name="whatsapp">
                                            <div class="invalid-feedback" id="whatsapp_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>PAN Verification</label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control text-uppercase" placeholder="Enter Pan" value="{{ old('pan_no') }}" name="pan_no" id="pan_no" />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_o_pan_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                               
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" id="pan_no_error"></div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="row p-3">
                                            <div class="col-md-6 col text-start">
                                                <a href="{{ route('register') }}" class="btn">
                                                    Previous
                                                </a>
                                            </div>
                                            <div class="col-md-6 col text-end">
                                                <button type="button" class="btn" id="submitForm">Next</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>

                            <!--/ End Form -->
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="contact-info">
                <div class="row">
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-ui-call"></i>
                            <div class="content">
                                <h3>+(000) 1234 56789</h3>
                                <p>info@company.com</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont-google-map"></i>
                            <div class="content">
                                <h3>2 Fir e Brigade Road</h3>
                                <p>Chittagonj, Lakshmipur</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-wall-clock"></i>
                            <div class="content">
                                <h3>Mon - Sat: 8am - 5pm</h3>
                                <p>Sunday Closed</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                </div>
            </div>
            <div class="modal fade" id="otpModal" tabindex="-1" role="dialog" aria-labelledby="otpModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="otpModalLabel">Enter OTP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="otpForm">
                            @csrf
                            
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="otpInput">OTP</label>
                                    <input type="text" class="form-control" value="" id="otpInput" placeholder="Enter OTP">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="submitOTP">Submit OTP</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
    <script>
  $(document).ready(function() {
    $('#otpForm').submit(function(e) {
        e.preventDefault(); // Prevent default form submission

        // Get the form data explicitly
        var formData = {
            'otp': $('#otpInput').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        };

        // Make an AJAX request to submit the form data
        $.ajax({
            url: "{{route('verifyOtp')}}",
            type: 'POST',
            data: formData,
            success: function(response) {
                console.log(response);
                $('#otpModal').modal('hide');
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});

     
        $(document).ready(function(){
            $('#verify_cin_btn').click(function(){
                var CINNumber = $('#cin').val();
                
                // Initiate the AJAX request to your Laravel route
                $.ajax({
                    url: "{{route('verify.cin')}}",
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'cin': CINNumber
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message === 'Verification successful. The CIN has been verified.') {
                            // Show verified message and disable button
                            $('#verify_cin_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_cin_btn').prop('disabled', false).text('CIN card not exit');
                        
                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    }
                });
            });
        });

        $('#verify_aadhar_btn').click(function() {
            var aadharNumber = $('#aadhar').val();
        
            // Initiate the AJAX request to your Laravel route
            $.ajax({
                url: "{{route('verify.o_aadhar')}}",
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'o_aadhar': aadharNumber // Corrected key name
                },
                success: function(response) {
                    console.log(response);
                    if (response.message === 'Verification successful. The Aadhar has been verified.') {
                    $('#otpModal').modal('show');
                    $('#verify_aadhar_btn').prop('disabled', true).text('Verified').removeClass('btn-primary').addClass('btn-success');
                    } else {
                        $('#verify_aadhar_btn').prop('disabled', false).text('Aadhar card does not exist').removeClass('btn-success').addClass('btn-primary');
                    }
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        });

        $(document).ready(function() {
            $('#verify_o_pan_btn').click(function() {
                var panNumber = $('#pan_no').val();

                // Initiate the AJAX request to your Laravel route
                $.ajax({
                    url: "{{route('verify_opan')}}",
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'o_pan_no': panNumber
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message ===
                            'Verification successful. The PAN has been verified.') {
                            // Show verified message and disable button
                            $('#verify_o_pan_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_o_pan_btn').prop('disabled', false).text(
                                'Pan card not exit');

                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    }
                });
            });
        });

        $(document).ready(function() {

$('#submitForm').click(function() {
    $.ajax({
        url: $('#organizationForm').attr('action'),
        type: 'POST',
        data: new FormData($('#organizationForm')[0]),
        processData: false,
        contentType: false,
        success: function(response) {
            console.log(response);
            // Handle success response

            // Redirect to account-verify route with organization id
            var organizationId = response.data;
            console.log(organizationId);
            window.location.href = "{{ route('home') }}";
        },
       

        error: function(xhr, status, error) {
                console.error('Error:', xhr.responseText); // Log error to console
                var errors = JSON.parse(xhr.responseText).errors;
                if (errors) {
                    $.each(errors, function(key, value) {
                        $('#' + key + '_error').text(value[0]);
                    });
                } else {
                    $('#erroraadhar').html(xhr.responseJSON.message); // Display error message
                }
            }
    });
});
});

function formatCardNumber() {
        var cardNumber = document.getElementById("aadhar");
        var value = cardNumber.value.replace(/\D/g, '');
        var formattedValue = "";
        for (var i = 0; i < value.length; i++) {
            if (i % 4 == 0 && i > 0) {
            formattedValue += " ";
            }
            formattedValue += value[i];
        }
        cardNumber.value = formattedValue;
        }


    </script>

 
@endsection
