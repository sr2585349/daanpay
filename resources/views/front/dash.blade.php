@extends('front.layouts.apps')

@section('content')

<div class="container my-5">

    <div id="ctf">
     <div id="ctfwrap">
      <div class="row">
       <div class="col-2" style="padding:0;">
    
        <nav id="ctfb1" class="navbar navbar-ctfb1 p-0">
         <ul class="nav flex-column nav-pills">
          <li><a data-toggle="tab" href="#Fields" class="">Fields</a></li>
          <li><a data-toggle="tab" href="#Create" class="">Create</a></li>
          <li><a data-toggle="tab" href="#Display" class="">Display</a></li>
          <li><a data-toggle="tab" href="#Views" class="">Views</a></li>
         </ul>
       </div>
    
       <div class="col-10" id="subcontent" style="padding:0;">
    <div class="container">
      
      <div class="tab-content">
          
         <div id="Fields" class="tab-pane active show">
            <div class="container tab-right">
             <div class="row">
              <div class="col-6">
                <div class="start-section">
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                 </div>
              </div>
              <div class="col-6">
                <div class="end-section">
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                    <p class="end-p"><strong>Name </strong>Lorem ipsum, dolor sit amet</p>
                </div>
              </div>
              </div>
              <hr>
             </div>
         </div>

         <div id="Create" class="tab-pane">
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Molestiae rem omnis sint temporibus quos eos quasi sunt explicabo sed, quia esse repellat nesciunt enim fugit consectetur molestias blanditiis minima eaque?</p>
          </div>


     
        </div>
       </div>
      </div>
     </div>
    </div>
    </div>
    
</div>

@endsection
