@extends('front.layouts.app')
@section('content')
<div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>Terms and Conditions</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">Terms and Conditions</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Start Portfolio Details Area -->
    <section class="pf-details section" style="text-align: justify;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="inner-content">
                        <!-- <div class="image-slider">
								<div class="pf-details-slider">
									<img src="img/call-bg.jpg" alt="#">
									<img src="img/call-bg.jpg" alt="#">
									<img src="img/call-bg.jpg" alt="#">
								</div>
							</div>
							<div class="date">
								<ul>
									<li><span>Category :</span> Heart Surgery</li>
									<li><span>Date :</span> April 20, 2019</li>
									<li><span>Client :</span> Suke Agency</li>
									<li><span>Ags :</span> Typo</li>
								</ul>
							</div> -->
                        <div class="body-text">
                            <h3 class="text-center">Terms and Conditions</h3>
                            <p>All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.</p>
                            <p>The Website Policies and Terms & Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore the Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modifications will be effective on the day they are posted.</p>
                            <p>Some of the advertisements you see on the Site are selected and delivered by third parties, such as ad networks, advertising agencies, advertisers, and audience segment providers. These third parties may collect information about you and your online activities, either on the Site or on other websites, through cookies, web beacons, and other technologies in an effort to understand your interests and deliver to you advertisements that are tailored to your interests. Please remember that we do not have access to, or control over, the information these third parties may collect. The information practices of these third parties are not covered by this privacy policy.</p>
                            <p>Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of India.</p>
                            <p>India is our country of domicile.</p>
                            <p>If you have any concern related to this Privacy Policy or collection, storage, retention or disclosure of Your Information under the terms of this Privacy Policy or Terms of Use or any other terms and conditions of Girnar or its other business entities including any queries or grievances, You can contact to Daanpay through its Data Protection Officer/Grievance Redressal Officer at the below given details:</p>
                            
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="inner-content">
                        <div class="body-text">
                            <p style="margin: 10px 0;"><strong style="font-size: 16px;">Name : </strong> Mrs.Bindeshwari devi</p>
                            <p style="margin: 10px 0;"><strong style="font-size: 16px;">E-mail:</strong> daanpay2021@gmail.com</p>
                            <p style="margin: 10px 0;"><strong style="font-size: 16px;">Telephone Number:</strong> 7303372233-9984984172</p>
                            <p style="margin: 10px 0;"><strong style="font-size: 16px;">Working Days:</strong> Monday to Friday</p>
                            <p style="margin: 10px 0;"><strong style="font-size: 16px;">Working Hours:</strong> 10:30 am to 6:30 pm</p>
                        </div>
                    </div>
                </div>
                <div class="body-text">
                 <p>We will strive to address your feedback and concerns in a timely and effective manner.</p>
                 <p>Please note that the details of the Data Protection Officer/Grievance Officer may be changed by us from time to time by updating this Privacy Policy.</p>
                </div>


            </div>
        </div>
    </section>
    <!-- End Portfolio Details Area -->
@endsection