@extends('front.layouts.app')
@section('content')


    <!-- Breadcrumbs -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>Registration</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">Registrationnn</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="loader-overlay">
    <div id="loader"></div>
</div>
</div>
    <!-- End Breadcrumbs -->
    <!-- Start Contact Us -->
    <section class="contact-us section">
        <div class="container">

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="inner">
                        <div class="contact-us-form">
                            <h2>Register Your Organization</h2>
                            <h6 id="erroraadhar" class="text-danger"></h6>
                            <!-- Form -->
                            <form class="form" id="organizationForm" method="post" action="{{ route('organization-data') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Organization Name</label>
                                            <input type="text" value="{{ old('o_name') }}" name="o_name" placeholder="Organization Name">
                                            <div class="invalid-feedback" id="o_name_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Organization Email</label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control" placeholder="Email" value="{{ old('o_email') }}" name="o_email" id="o_email" aria-label="Email" aria-describedby="button-addon1"  />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_o_email_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" id="o_email_error"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                          <label>Organization Category</label>
                                        <div class="form-group">
                                            <select name="organization_cat">
                                                <option value="">--select--</option>
                                                @foreach($organizationCat as $res)
                                                <option value="{{$res->id}}">{{$res->name}}</option>
                                                @endforeach
                                            </select>
                                        </div><br>
                                        <div class="invalid-feedback" id="organization_cat_error"></div>

                                    </div>
                                    <div class="col-lg-6">
                                    <label>Organization Phone/Mobile</label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control"  placeholder="Mobile Number" value="{{ old('o_mobile') }}" id="o_mobile" name="o_mobile" aria-label="Example input" aria-describedby="button-addon1" />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_o_mobile_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>

                                               
                                            </div>
                                        </div>
										<div class="invalid-feedback" id="o_mobile_error"></div>

									</div>
								</div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Organization PAN </label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control text-uppercase" placeholder="PAN" value="{{ old('o_pan_no') }}" name="o_pan_no" id="o_pan_no" aria-label="Example input" aria-describedby="button-addon1"  />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_o_pan_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback" id="o_pan_no_error"></div>
                                    </div>
                                     <div class="col-lg-6">
                                        <label>GST IN</label>
                                        <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control" value="{{ old('gst_in') }}" id="gst_in" name="gst_in" placeholder="GST IN No.">
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_gstIn_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                           
                                        </div>
                                        <div class="invalid-feedback" id="gst_in_error"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 mt-3">
                                        <div class="form-group">
                                            <label>IFSC Code</label>
                                            <input type="text"  value="{{ old('ifsc') }}"  id="ifsc" name="ifsc"
                                                placeholder="IFSC Code">
                                                <div class="invalid-feedback" id="ifsc_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 mt-3">
                                    <div class="form-group">
                                            <label>Account number</label>
                                    
                                            <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control" value="{{old('ac_no')}}" name="ac_no" id="ac_no" placeholder="Account Number" aria-label="Example input" aria-describedby="button-addon1" />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_account_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                            </div>
                                            <div class="invalid-feedback" id="ac_no_error"></div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 mt-3">
                                        <div class="form-group">
                                            <label>Organization Photo</label>
                                            <input type="file" value="{{ old('o_photo') }}" name="o_photo" placeholder="Organization Photo">
                                            <div class="invalid-feedback" id="o_photo_error"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 mt-3">
										<div class="form-group">
											<label>Zip</label>
											<input type="text" value="{{old('zip')}}" name="zip" placeholder="Zip" id="zip">

										</div>
									</div>
                                </div>
                                <div class="row">
                                <div class="col-lg-6">
										<div class="form-group">
											<label>City</label>
											<input type="text" name="city"  value="{{old('city')}}" placeholder="City"  id="city">

										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label>State</label>
											<input type="text" name="state"  value="{{old('state')}}"  placeholder="State"  id="state">

										</div>
									</div>
                                    <div class="col-lg-12">
                                        <div class="row p-3">
                                            <div class="col-md-6 col text-start">
                                               
                                                <button type="button" class="btn" id="submitForm">Next</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="contact-info">
                <div class="row">
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-ui-call"></i>
                            <div class="content">
                                <h3>+(000) 1234 56789</h3>
                                <p>info@company.com</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont-google-map"></i>
                            <div class="content">
                                <h3>2 Fir e Brigade Road</h3>
                                <p>Chittagonj, Lakshmipur</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-wall-clock"></i>
                            <div class="content">
                                <h3>Mon - Sat: 8am - 5pm</h3>
                                <p>Sunday Closed</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                </div>
            </div>

            <div class="modal fade" id="otpModal" tabindex="-1" role="dialog" aria-labelledby="otpModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="otpModalLabel">Enter OTP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="otpForm">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="otpInput">OTP</label>
                                    <input type="text" class="form-control" value="" id="otpInput" placeholder="Enter OTP">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="submitOTP">Submit OTP</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="mobileotpModal" tabindex="-1" role="dialog" aria-labelledby="mobileotpModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mobileotpModalLabel">Enter OTP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="mobileotpForm">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="otpInput">OTP</label>
                                    <input type="text" class="form-control" value="" id="mobileotpInput" placeholder="Enter OTP">
                                    <input type="hidden" name="referenceId" value="" id="referenceId">
                                    <input type="hidden" name="tsTransId" value="" id="tsTransId">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="submitMobileOTP">Submit OTP</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
     <script>

$(document).ready(function(){
    $("#zip").change(function(){ // Change event for zip input
        var zip = $(this).val();
        var csrfToken = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: "{{route('get.data')}}",
            method: 'post',
            data: {
                zip: zip,
                _token: csrfToken
            },
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            success: function(result) {
                console.log(result); // Log the response for debugging

                if(result === 'No') {
                    alert("Wrong PIN Code Entered!");
                    $("#state").val('');
                    $("#city").val('');
                } else {
                    $("#state").val(result.state);
                    $("#city").val(result.block);
                }
            },
            error: function(xhr, status, error) {
                console.error("AJAX Error:", xhr.responseText);
            }
        });
    });
});
     $(document).ready(function() {
    $('#verify_o_pan_btn').click(function() {
        var panNumber = $('#o_pan_no').val();
        $('#loader-overlay').show();

        // Initiate the AJAX request to your Laravel route
        $.ajax({
            url: "{{route('verify_opan')}}",
            type: 'POST',
            data: {
                '_token': '{{ csrf_token() }}',
                'o_pan_no': panNumber
            },
            success: function(response) {
                // Handle the response from the server
                console.log(response);
                // Update the UI or show a message based on the response
                if (response.message === 'Verification successful. The PAN has been verified.') {
                    // Show verified message and disable button
                    $('#verify_o_pan_btn').prop('disabled', true).text('Verified');
                } else {
                    // Show error message or handle other cases
                    $('#verify_o_pan_btn').prop('disabled', false).text('Pan card not exist');
                }
            },
            error: function(xhr, status, error) {
                // Handle errors, if any
                console.error(xhr.responseText);
            },
            complete: function() {
                // Hide full-screen loader overlay
                $('#loader-overlay').hide();
            }
        });
    });
});

$(document).ready(function(){
            $('#verify_gstIn_btn').click(function(){
                var GSTNumber = $('#gst_in').val();
                
                // Initiate the AJAX request to your Laravel route
                $.ajax({
                    url: "{{route('verify.gstin')}}",
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'gst_in': GSTNumber
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message === 'Verification successful. The GSTIN has been verified.') {
                            // Show verified message and disable button
                            $('#verify_gstIn_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_gstIn_btn').prop('disabled', false).text('GSTIN card not exit');
                        
                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    }
                });
            });
            });

            $(document).ready(function() {
            $('#verify_account_btn').click(function() {
                var AcNumber = $('#ac_no').val();
                var Ifsc = $('#ifsc').val();
                  $('#loader-overlay').show();
                $.ajax({
                    url: "{{route('verify.account')}}",
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'ac_no': AcNumber,
                        'ifsc' :Ifsc
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message ===
                            'Verification successful. The Account has been verified.') {
                            // Show verified message and disable button
                            $('#verify_account_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_account_btn').prop('disabled', false).text(
                                'Account Number not exit');

                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    },
                     complete: function() {
                        $('#loader-overlay').hide();
                    }
                });
            });
        });




$(document).ready(function() {
    $('#verify_o_aadhar_btn').click(function() {
        var aadharNumber = $('#o_aadhar').val();

        $('#loader-overlay').show();

        $.ajax({
            url: "{{route('verify.o_aadhar')}}",
            type: 'POST',
            data: {
                '_token': '{{ csrf_token() }}',
                'o_aadhar': aadharNumber
            },
            success: function(response) {
                console.log(response);
                if (response.message === 'Verification successful. The Aadhar has been verified.') {
                    $('#otpModal').modal('show');
                    $('#verify_o_aadhar_btn').prop('disabled', true).text('Verified').removeClass('btn-primary').addClass('btn-success');
                } else {
                    $('#verify_o_aadhar_btn').prop('disabled', false).text('Aadhar card does not exist').removeClass('btn-success').addClass('btn-primary');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
                // Handle error scenarios if needed
                $('#verify_o_aadhar_btn').prop('disabled', false).text('Verification Failed').removeClass('btn-success').addClass('btn-primary');
            },
            complete: function() {
                // Hide full-screen loader overlay
                $('#loader-overlay').hide();
            }
        });
    });
});
$(document).ready(function() {
    $('#verify_o_mobile_btn').click(function() {
        var Mobile = $('#o_mobile').val();

        $('#loader-overlay').show();

        $.ajax({
            url: "{{route('mobile.verity')}}",
            type: 'POST',
            data: {
                '_token': '{{ csrf_token() }}',
                'o_mobile': Mobile
            },
            success: function(response) {
                console.log(response);
                if (response.message === 'OTP sent successfully') {
                    $('#mobileotpModal').modal('show');
                    $('#referenceId').val(response[1]);
                    $('#tsTransId').val(response.tsTransId);
                    $('#verify_o_mobile_btn').prop('disabled', true).text('Verified').removeClass('btn-primary').addClass('btn-success');
                } else {
                    $('#verify_o_mobile_btn').prop('disabled', false).text('Mobile does not exist').removeClass('btn-success').addClass('btn-primary');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
                $('#verify_o_mobile_btn').prop('disabled', false).text('Verification Failed').removeClass('btn-success').addClass('btn-primary');
            },
            complete: function() {
                $('#loader-overlay').hide();
            }
        });
    });
});
$(document).ready(function() {
    $('#verify_o_email_btn').click(function() {
        var Email = $('#o_email').val();
        $('#loader-overlay').show();
        $.ajax({
            url: "{{route('verify.o_email')}}",
            type: 'POST',
            data: {
                '_token': '{{ csrf_token() }}',
                'o_email': Email
            },
            success: function(response) {
                console.log(response);
                if (response.message === 'Verification successful. The Email has been verified.') {
                    $('#verify_o_email_btn').prop('disabled', true).text('Verified').removeClass('btn-primary').addClass('btn-success');
                } else {
                    $('#verify_o_email_btn').prop('disabled', false).text('Email does not exist').removeClass('btn-success').addClass('btn-primary');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
                $('#verify_o_email_btn').prop('disabled', false).text('Verification Failed').removeClass('btn-success').addClass('btn-primary');
            },
            complete: function() {
                $('#loader-overlay').hide();
            }
        });
    });
});
       

        
$(document).ready(function() {
    $('#otpForm').submit(function(e) {
        e.preventDefault(); // Prevent default form submission

        // Get the form data explicitly
        var formData = {
            'otp': $('#otpInput').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        };

        // Make an AJAX request to submit the form data
        $.ajax({
            url: "{{route('verifyOtp')}}",
            type: 'POST',
            data: formData,
            success: function(response) {
                console.log(response);
                $('#otpModal').modal('hide');
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});
$(document).ready(function() {
    $('#mobileotpForm').submit(function(e) {
        e.preventDefault(); // Prevent default form submission

        // Get the form data explicitly
        var formData = {
            'otp': $('#mobileotpInput').val(),
            'reference_id':$('#referenceId').val(),
            'tsTransId':$('#tsTransId').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        };

        // Make an AJAX request to submit the form data
        $.ajax({
            url: "{{route('verifymobileOtp')}}",
            type: 'POST',
            data: formData,
            success: function(response) {

                if(response.message == 'Verification successful. The Mobile has been verified.')
                {
                    $('#mobileotpModal').modal('hide');
                }
                else{
                    
                }
              
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});

$(document).ready(function() {
    $('#submitForm').click(function() {
        $.ajax({
            url: $('#organizationForm').attr('action'),
            type: 'POST',
            data: new FormData($('#organizationForm')[0]),
            processData: false,
            contentType: false,
            success: function(response) {

                console.log(response); 
                var organizationId = response.data;
                console.log(organizationId);
                window.location.href = "{{ url('/account-verify') }}/" + organizationId;
            },
            error: function(xhr, status, error) {
                console.error('Error:', xhr.responseText); // Log error to console
                var errors = JSON.parse(xhr.responseText).errors;
                if (errors) {
                    $.each(errors, function(key, value) {
                        $('#' + key + '_error').text(value[0]);
                    });
                } else {
                    $('#erroraadhar').html(xhr.responseJSON.message); // Display error message
                }
            }
        });
    });
});



    </script> 

    <script type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=Your_Google_Key=places&callback=initAutocomplete"></script>
    <script>
        $(document).ready(function() {
            $("#latitudeArea").addClass("d-none");
            $("#longtitudeArea").addClass("d-none");
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                $('#latitude').val(place.geometry['location'].lat());
                $('#longitude').val(place.geometry['location'].lng());
                $("#latitudeArea").removeClass("d-none");
                $("#longtitudeArea").removeClass("d-none");
            });
        }

        function formatCardNumber() {
        var cardNumber = document.getElementById("o_aadhar");
        var value = cardNumber.value.replace(/\D/g, '');
        var formattedValue = "";
        for (var i = 0; i < value.length; i++) {
            if (i % 4 == 0 && i > 0) {
            formattedValue += " ";
            }
            formattedValue += value[i];
        }
        cardNumber.value = formattedValue;
        }

        
    </script>
@endsection
