@extends('front.layouts.app')

@section('content')

<div class="container mt-5">
    <table class="table table-responsive">
        <thead>
            <tr class="table-header">
                <th>Organization Name</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Payment Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($donations as $donation)
            <tr class="table-row">
                <td>{{ bladeOrganizationGetNameById($donation->organization_name) }}</td>
                <td>{{ $donation->amount }}</td>
                <td>{{ $donation->date }}</td>
                <td>{{ $donation->payment_status }}</td>
                <td><a href="{{ route('donation.reciept', $donation->donator_id) }}">Download Receipt</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
