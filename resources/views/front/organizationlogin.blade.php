@extends('front.layouts.app')
@section('content')

@if(session('message'))

	<h3 style="text-align: center; color:green">{{ session('message')}}</h3>
	
@endif
<div class="breadcrumbs overlay">
		<div class="container">
			<div class="bread-inner">
				<div class="row">
					<div class="col-12">
						<h2>Login</h2>
						<ul class="bread-list">
							<li><a href="#">Home</a></li>
							<li><i class="icofont-simple-right"></i></li>
							<li class="active">Login</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="loader-overlay">
    <div id="loader"></div>
</div>
	<!-- End Breadcrumbs -->

	<!-- Start Contact Us -->
	<section class="contact-us section">
		<div class="container">

			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="inner">
						<div class="contact-us-form">
							<h2>Login Your Organization</h2>
							@if(Session::has('success'))
							<div class="alert alert-success alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">×</button>
								{{Session::get('success')}}
							</div>
							@elseif(Session::has('failed'))
							<div class="alert alert-danger alert-dismissible">
								<button type="button" class="close" data-dismiss="alert">×</button>
								{{Session::get('failed')}}
							</div>
							@endif
							<form class="form" method="post">
								@csrf
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>Mobile</label>
											<input type="text" name="o_mobile" id="o_mobile" value="{{old('o_mobile')}}" placeholder="Enter Your Mobile">
											<small id="errorMobile" class="text-danger"></small>
											</div>
									</div>
								
									<div class="col-12">
										<div class="form-group login-btn">
											<button class="btn" id="otp_login" type="button">login</button>
										</div>
										
									</div>
								</div>
							</form>
							<!--/ End Form -->
						</div>
					</div>
					<div class="col-lg-2"></div>
				</div>
			</div>
			<div class="contact-info">
				<div class="row">
					<!-- single-info -->
					<div class="col-lg-4 col-12 ">
						<div class="single-info">
							<i class="icofont icofont-ui-call"></i>
							<div class="content">
								<h3>+(000) 1234 56789</h3>
								<p>info@company.com</p>
							</div>
						</div>
					</div>
					<!--/End single-info -->
					<!-- single-info -->
					<div class="col-lg-4 col-12 ">
						<div class="single-info">
							<i class="icofont-google-map"></i>
							<div class="content">
								<h3>2 Fir e Brigade Road</h3>
								<p>Chittagonj, Lakshmipur</p>
							</div>
						</div>
					</div>
					<!--/End single-info -->
					<!-- single-info -->
					<div class="col-lg-4 col-12 ">
						<div class="single-info">
							<i class="icofont icofont-wall-clock"></i>
							<div class="content">
								<h3>Mon - Sat: 8am - 5pm</h3>
								<p>Sunday Closed</p>
							</div>
						</div>
					</div>
					<!--/End single-info -->
				</div>
			</div>
		</div>

		
            <div class="modal fade" id="mobileotpModal" tabindex="-1" role="dialog" aria-labelledby="mobileotpModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mobileotpModalLabel">Enter OTP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="mobileotpForm">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="otpInput">OTP</label>
                                    <input type="text" class="form-control" value="" id="mobileotpInput" placeholder="Enter OTP">
                                    <input type="hidden" name="referenceId" value="" id="referenceId">
                                    <input type="hidden" name="tsTransId" value="" id="tsTransId">
                                    <input type="hidden" name="mobile" value="" id="mobile">
									<small id="error" class="text-danger"></small> <!-- Error message element -->
									

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="submitMobileOTP">Submit OTP</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
	</section>
	<!--/ End Contact Us -->
    <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>

	<script>
$(document).ready(function() {
    $('#otp_login').click(function() {
        var Mobile = $('#o_mobile').val();
        $('#loader-overlay').show();
        $.ajax({
            url: "{{ url('/verify-mobileotpLogin') }}",
            type: 'POST',
            data: {
                '_token': '{{ csrf_token() }}',
                'o_mobile': Mobile
            },
            success: function(response) {
                console.log(response);
                if (response.message === 'OTP sent successfully') {
                    $('#mobileotpModal').modal('show');
                    $('#referenceId').val(response.referenceId);
                    $('#tsTransId').val(response.tsTransId);
                    $('#mobile').val(response.mobile);
                } else {
                    $('#errorMobile').html(response.message); // Show the error message returned from backend
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
                // Handle AJAX errors if needed
            },
            complete: function() {
                $('#loader-overlay').hide();
            }
        });
    });
});





$(document).ready(function() {
    $('#mobileotpForm').submit(function(e) {
        e.preventDefault(); // Prevent default form submission

        // Get the form data explicitly
        var formData = {
            'otp': $('#mobileotpInput').val(),
            'reference_id':$('#referenceId').val(),
            'tsTransId':$('#tsTransId').val(),
            'mobile':$('#mobile').val(),
            '_token': $('meta[name="csrf-token"]').attr('content')
        };

        // Make an AJAX request to submit the form data
        $.ajax({
            url: "{{route('loginmobileOtp')}}",
            type: 'POST',
            data: formData,
            success: function(response) {

                if(response.message == 'Login successful')
                {
                    $('#mobileotpModal').modal('hide');
					window.location.href = "{{ route('organizationDashboard') }}";
                }
                else {
					$('#error').text('Invalid OTP'); // Add error message for invalid OTP
				}
              
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
});
	</script>

@endsection