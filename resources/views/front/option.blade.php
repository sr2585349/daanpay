@extends('front.layouts.app')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>Registration</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">Registration</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    <!-- Start Contact Us -->
    <section class="contact-us section">
        <div class="container">

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="inner">
                        <div class="contact-us-form">
                            <h5 class="text-center my-5">Register now to join our community and start your journey with us!</h5>
                            <div class="row">
                                   <div class="col-lg-3 offset-md-2">
									<div class="row p-3 ">
										<div class="col-md-6 col text-start">
										<a href="{{route('user-register')}}" class="btn text-white">
												User Registration
											</a>
										</div>

									</div>
								   </div>

                                   <div class="col-lg-6 px-5">
									<div class="row p-3">
										<div class="col-md-6 col text-start">
										<a href="{{route('register')}}" class="btn text-white">
												Organiser Registration
											</a>
										</div>

									</div>
								   </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="contact-info">
                <div class="row">
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-ui-call"></i>
                            <div class="content">
                                <h3>+(000) 1234 56789</h3>
                                <p>info@company.com</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont-google-map"></i>
                            <div class="content">
                                <h3>2 Fir e Brigade Road</h3>
                                <p>Chittagonj, Lakshmipur</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-wall-clock"></i>
                            <div class="content">
                                <h3>Mon - Sat: 8am - 5pm</h3>
                                <p>Sunday Closed</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
    {{-- <script>
        $(document).ready(function() {
            $('#verify_o_pan_btn').click(function() {
                var panNumber = $('#o_pan_no').val();

                // Initiate the AJAX request to your Laravel route
                $.ajax({
                    url: '/daanPay/verify-opan',
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'o_pan_no': panNumber
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message ===
                            'Verification successful. The PAN has been verified.') {
                            // Show verified message and disable button
                            $('#verify_o_pan_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_o_pan_btn').prop('disabled', false).text(
                                'Pan card not exit');

                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    }
                });
            });
        });
        $(document).ready(function() {
            $('#').click(function() {
                var panNumber = $('#pan_no').val();

                // Initiate the AJAX request to your Laravel route
                $.ajax({
                    url: '/daanPay/verify-pan',
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'pan_no': panNumber
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message ===
                            'Verification successful. The PAN has been verified.') {
                            // Show verified message and disable button
                            $('#verify_pan_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_pan_btn').prop('disabled', false).text(
                                'Pan card not exit');

                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    }
                });
            });
        });




        $(document).ready(function() {
            $('#submit-btn').click(function(e) {
                e.preventDefault();
                var formData = new FormData($('#signup-form')[0]); // Create FormData object
                $.ajax({
                    type: 'POST',
                    url: $('#signup-form').attr('action'),
                    data: formData,
                    processData: false, // Prevent jQuery from processing the data
                    contentType: false, // Prevent jQuery from setting the Content-Type
                    success: function(response) {
                        window.location.href = "{{ route('login') }}";
                    },
                    error: function(xhr, status, error) {
                        var errors = JSON.parse(xhr.responseText);
                        $.each(errors.errors, function(key, value) {
                            $('#' + key + '_error').text(value);
                        });
                    }
                });
            });
        });



        $(document).ready(function() {
            $("#zip").change(function() { // Change event for zip input
                var zip = $(this).val();
                var csrfToken = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    url: "{{ route('get.data') }}",
                    method: 'post',
                    data: {
                        zip: zip,
                        _token: csrfToken
                    },
                    headers: {
                        'X-CSRF-TOKEN': csrfToken
                    },
                    success: function(result) {
                        console.log(result); // Log the response for debugging

                        if (result === 'No') {
                            alert("Wrong PIN Code Entered!");
                            $("#state").val('');
                            $("#city").val('');
                        } else {
                            $("#state").val(result.state);
                            $("#city").val(result.block);
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error("AJAX Error:", xhr.responseText);
                    }
                });
            });
        });
    </script> --}}

    <script type="text/javascript"
        src="https://maps.google.com/maps/api/js?key=Your_Google_Key=places&callback=initAutocomplete"></script>
    <script>
        $(document).ready(function() {
            $("#latitudeArea").addClass("d-none");
            $("#longtitudeArea").addClass("d-none");
        });
    </script>
    <script>
        google.maps.event.addDomListener(window, 'load', initialize);

        function initialize() {
            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                $('#latitude').val(place.geometry['location'].lat());
                $('#longitude').val(place.geometry['location'].lng());
                $("#latitudeArea").removeClass("d-none");
                $("#longtitudeArea").removeClass("d-none");
            });
        }
    </script>
@endsection
