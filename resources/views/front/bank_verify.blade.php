@extends('front.layouts.app')
@section('content')
<style>
#loader-overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5); /* Semi-transparent black overlay */
    z-index: 9999; /* Ensure it's above everything else */
    display: none; /* Initially hidden */
}

#loader {
    border: 16px solid #f3f3f3; /* Light grey */
    border-top: 16px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -60px; /* Half of width */
    margin-top: -60px; /* Half of height */
}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}

</style>
    <!-- Breadcrumbs -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>Account Verification</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li><a href="{{route('register')}}">Registration</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li><a href="{{route('account-verify',$id)}}">Account Verification</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">Bank Verification</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div id="loader-overlay">
    <div id="loader"></div>
</div>
    <!-- End Breadcrumbs -->
    <!-- Start Contact Us -->
    <section class="contact-us section">
        <div class="container">

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="inner">
                        <div class="contact-us-form">
                            <h2>Bank Verification</h2>
                            <h6 id="erroraadhar" class="text-danger"></h6>
                            <!-- Form -->
                            <form class="form" id="organizationForm" method="post" action="{{ route('bank-data') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>IFSC Code</label>
                                            <input type="text"  value="{{ old('ifsc') }}"  id="ifsc" name="ifsc"
                                                placeholder="IFSC Code">
                                                <div class="invalid-feedback" id="ifsc_error"></div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" value="{{$id}}">

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Account number (verification of bank account)</label>
                                    
                                            <div class="d-flex justify-content-center d-flx-btn">
                                            <div class="input-group w-auto">
                                                <input type="text" class="form-control" value="{{old('ac_no')}}" name="ac_no" id="ac_no" placeholder="Account Number" aria-label="Example input" aria-describedby="button-addon1" />
                                                <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary" type="button" id="verify_account_btn" data-mdb-ripple-color="dark">
                                                    Verify
                                                </button>
                                            </div>
                                            <div class="invalid-feedback" id="ac_no_error"></div>
                                            </div>
                                        </div>
                                    </div>

                                    


                                   <div class="col-lg-12">
									<div class="row p-3">
										<div class="col-md-6 col text-start">
                                            <a href="{{route('account-verify',$id)}}" class="btn">
												Previous
											</a>
										</div>
									
                                        <button type="button" class="btn" id="submitForm">Submit</button>	
									</div>
								   </div>

                                </div>
                               			

                            </form>
                            <!--/ End Form -->
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="contact-info">
                <div class="row">
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-ui-call"></i>
                            <div class="content">
                                <h3>+(000) 1234 56789</h3>
                                <p>info@company.com</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont-google-map"></i>
                            <div class="content">
                                <h3>2 Fir e Brigade Road</h3>
                                <p>Chittagonj, Lakshmipur</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-wall-clock"></i>
                            <div class="content">
                                <h3>Mon - Sat: 8am - 5pm</h3>
                                <p>Sunday Closed</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#verify_account_btn').click(function() {
                var AcNumber = $('#ac_no').val();
                var Ifsc = $('#ifsc').val();
                  $('#loader-overlay').show();
                $.ajax({
                    url: "{{route('verify.account')}}",
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        'ac_no': AcNumber,
                        'ifsc' :Ifsc
                    },
                    success: function(response) {
                        // Handle the response from the server
                        console.log(response);
                        // Update the UI or show a message based on the response
                        if (response.message ===
                            'Verification successful. The Account has been verified.') {
                            // Show verified message and disable button
                            $('#verify_account_btn').prop('disabled', true).text('Verified');
                        } else {
                            // Show error message or handle other cases
                            $('#verify_account_btn').prop('disabled', false).text(
                                'Account Number not exit');

                        }
                    },
                    error: function(xhr, status, error) {
                        // Handle errors, if any
                        console.error(xhr.responseText);
                    },
                     complete: function() {
                        $('#loader-overlay').hide();
                    }
                });
            });
        });

    $('#submitForm').click(function() {
        $.ajax({
            url: $('#organizationForm').attr('action'),
            type: 'POST',
            data: new FormData($('#organizationForm')[0]),
            processData: false,
            contentType: false,
            success: function(response) {
                window.location.href = "{{ route('home') }}";
            },
            error: function(xhr, status, error) {
                    console.error('Error:', xhr.responseText); // Log error to console
                    var errors = JSON.parse(xhr.responseText).errors;
                    if (errors) {
                        $.each(errors, function(key, value) {
                            $('#' + key + '_error').text(value[0]);
                        });
                    } else {
                        $('#erroraadhar').html(xhr.responseJSON.message); // Display error message
                    }
                }
        });
    });
 
    </script>


@endsection
