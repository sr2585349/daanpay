@extends('front.layouts.app')
@section('content')


<div class="breadcrumbs overlay">
		<div class="container">
			<div class="bread-inner">
				<div class="row">
					<div class="col-12">
						<h2>Login</h2>
						<ul class="bread-list">
							<li><a href="#">Home</a></li>
							<li><i class="icofont-simple-right"></i></li>
							<li class="active">Login</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="loader-overlay">
    <div id="loader"></div>
</div>
	<!-- End Breadcrumbs -->

	<!-- Start Contact Us -->
	<section class="contact-us section">
		<div class="container">

			<div class="row">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="inner">
						<div class="contact-us-formm">
							<div class="row">
								<div class="col-md-3 offset-md-2 col-6 icon-box text-center">
									<img src="{{ asset('assets/front/fonts/user-login.svg') }}" alt="SVG Icon" height="50px" width="50px"/><br>
									<a href="{{route('userlogin')}}" class="mt-2">User Login</a><br>
								</div>
								<div class="col-md-3 offset-md-1 col-6  icon-box text-center">
									<img src="{{ asset('assets/front/fonts/Organization-login.svg') }}" alt="SVG Icon" height="50px" width="50px"/><br>
									<a href="{{route('organizationLogin')}}" class="mt-2">Organization Login</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2"></div>
				</div>
			</div>
			<div class="contact-info">
				<div class="row">
					<!-- single-info -->
					<div class="col-lg-4 col-12 ">
						<div class="single-info">
							<i class="icofont icofont-ui-call"></i>
							<div class="content">
								<h3>+(000) 1234 56789</h3>
								<p>info@company.com</p>
							</div>
						</div>
					</div>
					<!--/End single-info -->
					<!-- single-info -->
					<div class="col-lg-4 col-12 ">
						<div class="single-info">
							<i class="icofont-google-map"></i>
							<div class="content">
								<h3>2 Fir e Brigade Road</h3>
								<p>Chittagonj, Lakshmipur</p>
							</div>
						</div>
					</div>
					<!--/End single-info -->
					<!-- single-info -->
					<div class="col-lg-4 col-12 ">
						<div class="single-info">
							<i class="icofont icofont-wall-clock"></i>
							<div class="content">
								<h3>Mon - Sat: 8am - 5pm</h3>
								<p>Sunday Closed</p>
							</div>
						</div>
					</div>
					<!--/End single-info -->
				</div>
			</div>
		</div>

		
            <div class="modal fade" id="mobileotpModal" tabindex="-1" role="dialog" aria-labelledby="mobileotpModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="mobileotpModalLabel">Enter OTP</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="mobileotpForm">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="otpInput">OTP</label>
                                    <input type="text" class="form-control" value="" id="mobileotpInput" placeholder="Enter OTP">
                                    <input type="hidden" name="referenceId" value="" id="referenceId">
                                    <input type="hidden" name="tsTransId" value="" id="tsTransId">
                                    <input type="hidden" name="mobile" value="" id="mobile">
									<small id="error" class="text-danger"></small> <!-- Error message element -->
									

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary" id="submitMobileOTP">Submit OTP</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
	</section>


@endsection