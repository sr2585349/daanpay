@extends('front.layouts.app')
@section('content')
<div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>Privacy Policy</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">Privacy Policy</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->

    <!-- Start Portfolio Details Area -->
    <section class="pf-details section" style="text-align: justify;">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="inner-content">
                        <!-- <div class="image-slider">
								<div class="pf-details-slider">
									<img src="img/call-bg.jpg" alt="#">
									<img src="img/call-bg.jpg" alt="#">
									<img src="img/call-bg.jpg" alt="#">
								</div>
							</div>
							<div class="date">
								<ul>
									<li><span>Category :</span> Heart Surgery</li>
									<li><span>Date :</span> April 20, 2019</li>
									<li><span>Client :</span> Suke Agency</li>
									<li><span>Ags :</span> Typo</li>
								</ul>
							</div> -->
                        <div class="body-text">
                            <h3 class="text-center">Privacy Policy</h3>
                             <p>THIS PRIVACY POLICY IS AN ELECTRONIC RECORD IN THE FORM OF ELECTRONIC CONTRACT IN TERMS OF INDIAN INFORMATION TECHNOLOGY ACT, 2000 AND RULES MADE THEREUNDER (AS AMENDED FROM TIME TO TIME) AND DOES NOT REQUIRE ANY PHYSICAL SIGNATURE OR SEAL.</p>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>
    <!-- End Portfolio Details Area -->
@endsection