@extends('front.layouts.app')
@section('content')
    <!-- Breadcrumbs -->
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="bread-inner">
                <div class="row">
                    <div class="col-12">
                        <h2>User Registration</h2>
                        <ul class="bread-list">
                            <li><a href="index.html">Home</a></li>
                            <li><i class="icofont-simple-right"></i></li>
                            <li class="active">User Registration</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Breadcrumbs -->
    <!-- Start Contact Us -->
    <section class="contact-us section">
        <div class="container">

            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="inner">
                        <div class="contact-us-form">
                            <h2>User Register</h2>
                            <!-- Form -->
                            <form  class="form" method="POST" action="{{ route('userRegister') }}" >
                                @csrf
                                <div class="row">
                                    <div class="col-lg-10 offset-md-1">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" value="{{old('name')}}" name="name"
                                                placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-10 offset-md-1">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text"  value="{{old('email')}}" name="email"
                                                placeholder="Enter Email">
                                        </div>
                                    </div>
                                    <div class="col-lg-10 offset-md-1">
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input type="text" value="{{old('mobile')}}" name="mobile"
                                                placeholder="Enter Phone No.">
                                        </div>
                                    </div>
                                    <div class="col-lg-10 offset-md-1">
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" value="{{old('password')}}" name="password"
                                                placeholder="Enter Password">
                                        </div>
                                    </div>
                                   <div class="col-lg-10 offset-md-1">
									<div class="row p-3">
										<div class="col-md-6 col text-start">
										<button type="submit" class="btn">
												Submit
											</a>
										</div>

									</div>
								   </div>

                                </div>
                            </form>
                            <!--/ End Form -->
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="contact-info">
                <div class="row">
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-ui-call"></i>
                            <div class="content">
                                <h3>+(000) 1234 56789</h3>
                                <p>info@company.com</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont-google-map"></i>
                            <div class="content">
                                <h3>2 Fir e Brigade Road</h3>
                                <p>Chittagonj, Lakshmipur</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                    <!-- single-info -->
                    <div class="col-lg-4 col-12 ">
                        <div class="single-info">
                            <i class="icofont icofont-wall-clock"></i>
                            <div class="content">
                                <h3>Mon - Sat: 8am - 5pm</h3>
                                <p>Sunday Closed</p>
                            </div>
                        </div>
                    </div>
                    <!--/End single-info -->
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
 

@endsection
