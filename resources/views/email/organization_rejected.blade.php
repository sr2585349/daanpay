<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Organization Rejected</title>
</head>
<body>
    <p>Dear {{ $organizationName }},</p>
    
    <p>We regret to inform you that your organization's application has been rejected.</p>
    
    <p>Thank you for your interest.</p>
    
    <p>Regards,</p>
    <p>Your Organization Team</p>
</body>
</html>
