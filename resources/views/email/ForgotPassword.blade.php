<h1>Forget Password Email Message</h1>
<p>You can reset the password for <a href="{{ route('home') }}">TAPFYBO</a> Login. Click the link: <a href="{{ route('reset.password.get', $token) }}">Reset Password</a></p>
<p>Team <br>TAPFYBO</p>